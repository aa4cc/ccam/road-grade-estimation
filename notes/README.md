# Závěry z konzultací - bakalářská práce

## Konzultace 6.10.2022

Úkoly:
1. ~~Předělat repozitář, aby měl nějakou systematičtější strukturu. Například podle těchto instrukcí/rad.~~
2. ~~Připravit kód, který data z experimentu uložená coby CSV načte do matlabského formátu timetable. Tohle mimo jiné umožní mít pak ve stejné tabulce jak měření z akcelerometru, tak právě i to měření z odometrie.~~
3. ~~Seznámit se s EVAL-ADIS-F3X. Do něj použít senzorický modul (na PCB desce) ze žluté krabičky s EVAL-ADIS2. Prozkoumat, zda se dají ukládat časové značky.~~
4. ~~Do závěrečné zprávy z letního projektu vložit i obrázek s parametrizací sklonu ujetou dráhou. A mít taková data přístupná i v repozitáři.~~


navíc:
- S Loiem prodiskutovány street mapy: Dostali jsme přístup do repozitáře se zdrojovými kódy na zpracování náhledu mapy.

Poznámky k úkolům:
1. bez problémů
2. V zpracovaných datech u gyroskopu změněy jednotky.
3. Timestamp se nepovedl zprovoznit. Navíc kvůli odlišným rozměrům evaluačního kitu, bychom nebyli schopni použít již vytisknutou žlutou krabičku. To nás vedlo k namodelování nové krabičky na novou evaluační desku, která je zamýšlená už na nový set senzoru s eval.
4. bez problémů


## Konzultace 20.09.2022

Úkoly:
1. readmatrix misto readtable v datatimetable
2. vytahnout ze street map elevaci - konzultace s Loiem
3. rfid znacky
4. rozvrhnuti planu experimentu vuci profilu trasy - ziskat profil trasy
5. model imu senzoru, tramvaje - model based
6. system object - matlab na drivery
7. pro mapu dostavat polohu - kluci matej s jakubem - konzultace
8. real time brat s rezervou 
9. system object - Loi, Martin Gurtner(1 za tyden)
10. imu s raspberry pi 
11. předzpracovavani dat pro nasledny mensi objem dat - raspberry pi
12. před experiment v autě
13. pěna pod deskou v tramvaji (?) - pogooglit uchyceni krabicky

## Bakalářský projekt (poznatky, zajímavé odkazy, TODO):

### IMU propojení s Matlabem (driver)

Cíl by byl real time sběr dat z našeho IMU(spíše evaluačního boardu) do matlabu. 
Zatím ze zjištěných informací bude potřeba napsat dynamickou knihovnu .dll formátu (nejsem si jistý jestli bude fungovat na windows i Linux).
Ještě je potřeba rozhodnout pro jaký evaluační kit budeme tuto knihovnu psát EVAL-ADIS2(+ ADIS 16465-1bmlz)/EVAL-ADIS-FX3(+ADIS 16505-1/PCBZ).

Odkazy:
1. Takováhle knihovna už existuje, ale podporuje pouze 32bit Verzi Matlabu (pro to pro nás nepoužitelná na Matlab 32bit nikdo přecházet nebudeme): https://ez.analog.com/mems/w/documents/4483/faq-eval-adis-in-matlab , https://ez.analog.com/mems/w/documents/4474/faq-eval-adis-data-streaming-into-matlab
2. Gitlab, kde je popsana komunikace s různými typy adis (ani jeden nemáme): https://github.com/analogdevicesinc/MathWorks_tools/tree/master/%2Badi
3. SPI komunikace v Matlabu: https://www.mathworks.com/help/instrument/spi-communication.html , https://la.mathworks.com/help/supportpkg/texasinstrumentsc2000/ug/using-spi-to-read-write-data-spi-eeprom.html
4. ADI's IMU + raspberry pi a Matlab (zajímavý pohled na věc) - ale vypadá to, že využívá dll z 2. odkazu: https://www.youtube.com/watch?v=G-k81W2yJRY&ab_channel=CircuitsfromtheLab


### Mapy propojení s Matlabem

Cíl by byl vytvořit mapu Prahy (rozkouskovanou po úsecích), ve které by byly vyznačeny GPS lokace tramvajových linek, a elevační údaje. Úkol se zdá být poměrně komplexní hlavně co se týče elevačních údajů a jelikož máme z různých zdrojů ověřené, že údaje o elevacích stejně neodpovídají skutečnosti, tak se vyplatí zvážit, jestli to má cenu. Nýbrž mapa s údajemi o tramvajových tras asi bude potřeba pro následný úložení dat. (za předpokladu, že se nám nepovede dostat téměř perfektní údaje z GPS - o což se snaží kolegové Jakub Kašpar a Matěj Kříž)

Odkazy: 
1. street mapy pro lokace kolejí: https://www.openstreetmap.org/#map=8/49.817/15.478
2. Export z openstreet map větších objektů (celé Prahy): https://export.hotosm.org/en/v3/exports/new/describe 