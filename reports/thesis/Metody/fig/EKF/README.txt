koncovky:

- 1:
	% initialize EKF
	% pro h
	%trueInitialState = [0.00001;121;0.00001];
	% pro a 
	trueInitialState = [0.00001;329.3;0.00001]; % [v,z,alpha]
	initialCovariance = diag([1,0.001,0.001]);
	processNoise = diag([1; 1; 1]); % Process noise matrix
	measureNoise = diag([1500000;900000]); % Measurement noise matrix. Units are m^2 and rad^2.
	
	RMSE_a = 0.0058
	RMSE_h = 0.0102