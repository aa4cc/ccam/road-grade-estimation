%!TEX ROOT=main.tex

\section{Luenbergerův pozorovatel}
\label{sec:Luenberg}
Jako první metodu jsem zvolil implementaci metody popsané v~\cite{automotive}, která využívá pro odhad sklonu Luenbergerova pozorovatele. 
Pozorovatel bude implementován pomocí schématu zobrazeného na obr.~\ref{fig:Luenberger_scheme}.
Měřený stav je rychlost vozidla $v$. Dále je využita síla motoru $F_{\textup{motoru}}$. 
\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{IPE_drawings/luenberg.pdf}
    \caption{Struktura Luenbergerova pozorovatele}
    \label{fig:Luenberger_scheme}
\end{figure}

\subsection{Linearizace modelu}
Pro implementaci stavového pozorovatele je potřeba model~\eqref{eq:model} zlinearizovat, proto jsem zavedl dva předpoklady: 

\begin{itemize}
    \item Jelikož koleje tramvají se nachází na silnicích s malými sklony budu předpokládat, že $\sin(\alpha) \approx \alpha$.
    \item Pro eliminaci mocniny v odporu vzduchu si vytvořím novou proměnnou a to sice $F_{\textup{v}}$, kde $F_{\textup{v}} = F_{\textup{motoru}} - F_{\textup{vzduch}} - F_{\textup{odpor}}$. 
    Celá tato výsledná síla bude použita jako vstup do modelu.  
\end{itemize}
Výsledkem linearizace je zjednodušení formy rovnice~\eqref{eq:model} na tvar

\begin{equation}
    m \dot{v} = F_{\textup{v}} - m g  \alpha \; .
\label{eq:Luenberger_model}
\end{equation}

\par 
Stavový vektor $x$ bude obsahovat rychlost vozidla $v$ a sklon tratě $\alpha$. 
Vektor vstupu $u$ bude obsahovat výslednou sílu $F_{\textup{v}}$ a $0$. 
Druhý vstup je do modelu přidán, aby bylo možné přidat injekci z výstupu (termín vysvětlen v rovnici~\eqref{eq:Luenberger_observer}) i do druhého odhadu stavu $\hat{\alpha}$.
\begin{equation}
    x = \begin{bmatrix}
        v \\
        \alpha 
        \end{bmatrix} , 
        \qquad 
        u = \begin{bmatrix}
            F_{\textup{v}} \\
            0
            \end{bmatrix}\; .
\end{equation}
Výsledný lineární model ve tvaru 
\begin{equation}
    \begin{aligned}
        & \dot{x} = \textup{A} x + \textup{B} u \; , \\ 
        & y = \textup{C} x \; ,
    \end{aligned}
\end{equation}

bude vyčíslen následovně
\begin{equation}
    \begin{aligned}
        &\begin{bmatrix}
            \dot{v} \\
            \dot{\alpha} 
        \end{bmatrix} = 
        \begin{bmatrix}
            0 & -g \\
            0 & 0 
            \end{bmatrix}
        \begin{bmatrix}
            v \\
            \alpha 
            \end{bmatrix}
        +
        \begin{bmatrix}
            \frac{1}{m} & 0\\
            0 & 0 
            \end{bmatrix}
        \begin{bmatrix}
            F_\textup{v} \\
            0 
            \end{bmatrix} \; ,\\ 
        &y = \begin{bmatrix}
            1 & 0
            \end{bmatrix}
            \begin{bmatrix}
                v \\
                \alpha 
                \end{bmatrix} \;.
    \end{aligned}
    \label{eq:SSmodel_liearized}
\end{equation}

\subsection{Návrh pozorovatele}
Pro možnost navržení pozorovatele je zapotřebí, aby systém~\eqref{eq:SSmodel_liearized} byl pozorovatelný. 
Tento předpoklad zjistím pomocí matice pozorovatelnosti, která bude v našem případě
\begin{equation}
    \mathcal{O} = \begin{bmatrix}
        \textup{C} \\
        \textup{C \textup{A}} 
    \end{bmatrix} = \begin{bmatrix}
        1 & 0 \\
        0 & -g
    \end{bmatrix},
\end{equation}
kde hodnost matice $\mathcal{O}$ je 2. To pro systém 2. řádu včetně mého znamená, že je plně pozorovatelný a návrh pozorovatele je proto možný.

\par
Luenbergerův pozorovatel se zapisuje jako 
\begin{equation}
    \begin{aligned}
        & \dot{\hat{x}} = \textup{A} \hat{x} + \textup{B} u + \underbrace{\textup{L} (y - \hat{y})}_{\textup{injekce z výstupu}} \; ,\\ 
        & \hat{y} = \textup{C} \hat{x} \; .
    \end{aligned}
    \label{eq:Luenberger_observer}
\end{equation}
K chybě pozorovatele se dostaneme následujícími úpravami rovnic~\eqref{eq:SSmodel_liearized} a~\eqref{eq:Luenberger_observer}
\begin{equation}
    \begin{aligned}
    & e = x - \hat{x} \; ,\\
    & \dot{e} = \dot{x} - \dot{\hat{x}} \; ,\\
    & \dot{e} = (\textup{A}x + \textup{B}u) - (\textup{A}\hat{x} + \textup{B}u + \textup{L}(y-\hat{y})) \; , \\
    & \dot{e} = (\textup{A}-\textup{L}\textup{C}) e \; . 
    \end{aligned}
\end{equation}
Vlastní čísla matice $(\textup{A}-\textup{L}\textup{C})$ nastavím pomocí matice $\textup{L}$, která bude mít dvě hodnoty.
Po transpozici dostaneme $(\textup{A}^\textup{T}-\textup{C}^\textup{T}\textup{L}^\textup{T})$.
Nyní lze využít duální úlohu k umisťování pólů matice $(\textup{A}-\textup{B}\textup{K})$ 
pro kterou lze využít funkce \textit{place} v Matlabu\footnote[1]{Place: Pole placement design [online]. [cit. 2023-05-21]. \newline Dostupné z: https://www.mathworks.com/help/control/ref/place.html}.
\par 
Pro umístění pólů se podívám na póly svého systému, které dostanu jako vlastní čísla matice $\textup{A}$ jako
\begin{equation}
    \textup{eig}(\textup{A}) = \begin{bmatrix}
        0 \\
        0 
    \end{bmatrix} \; ,
\end{equation}
póly jsem umístil o $0.6$ a $0.7$ v záporném směru reálné osy. Výsledná matice  
\begin{equation}
    \textup{L} = \begin{bmatrix}
        1.3 \\
        -0.0428  
    \end{bmatrix} \; .
\end{equation}

\newpage
\subsection{Výsledky}
Referenční rychlost budu v jednotlivých metodách implementovat jako součet konstanty $25 \, \si{\meter\per\second}$ a 
obdélníkového signálu s amplitudou $A = 5 \, \si{\meter\per\second}$ a~periodou $T = 10 \, \textup{s}$.
Na grafech~\ref{fig:v_ref} je ukázána referenční rychlost, reálná rychlost vozidla a nakonec rychlost zatížená nejistotou měření.
Tento způsob je zaveden kvůli přiblížení se reálné jízdě vozidla, kdy se řidič (v mém případě PID regulátor)
musí podřídit aktuální jízdní situaci a nikdy nebude požadovaná rychlost konstantních $25 \, \si{\meter\per\second}$. 
\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{Metody/fig/Luenberg/v_ref_profil_h.eps}
        \caption{Požadovaná referenční rychlost pro 1. profil}
        \label{}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{Metody/fig/Luenberg/v_ref_profil_a.eps}
        \caption{Požadovaná referenční rychlost pro 2. profil}
        \label{}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{Metody/fig/Luenberg/v_real_profil_h.eps}
        \caption{Rychlost modelu s/bez nejistoty měření pro 1. profil}
        \label{}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{Metody/fig/Luenberg/v_real_profil_a.eps}
        \caption{Rychlost modelu s/bez nejistoty měření pro 2. profil}
        \label{}
    \end{subfigure}
    \caption{Požadovaná referenční rychlost simulující reálný provoz}
    \label{fig:v_ref}
\end{figure}


\par
Na obr.~\ref{fig:Luenberger_estimates} lze vidět, že navržený pozorovatel při ideálním čtení rychlosti (tj. rychlost bez šumu) 
kopíruje profil tratě zadaný modelu s relativně malou odchylkou.
\begin{figure}[htb!]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Metody/fig/Luenberg/luenberg_profil_h_no_noise.eps}
		\caption{Odhad Luenbergerova pozorovatele 1. profil bez nejistoty měření rychlosti}
		\label{fig:Luenberger_noinitial}
	\end{subfigure}%
	~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Metody/fig/Luenberg/luenberg_profil_a_no_noise.eps}
		\caption{Odhad Luenbergerova pozorovatele 2. profil bez nejistoty měření rychlosti}
		\label{fig:Luenberger_initial}
	\end{subfigure}%
	\caption{Luenbergerův pozorovatel: porovnání odhadu sklonu s reálným sklonem při měření bez šumu}
	\label{fig:Luenberger_estimates}
\end{figure}

Dále na obr.~\ref{fig:Luenberger_estimates_noise} lze vidět případ, kdy k rychlosti vygenerované z modelu byla přičtena nejistota popsaná v sekci~\ref{sec:noise}. 
Když se podíváme na odhad sklonu, tak vidíme, že pozorovatel sleduje profil s poměrně znatelnou odchylkou. 
\par
Z výše uvedených poznatků vidíme, že tímto způsobem navržený Luenbergerův pozorovatel je vhodný pouze pro případy, kdy bychom měli
bezchybné čtení ze senzorů bez šumových vlastností, což v reálném případě není prakticky možné.
\begin{figure}[htb!]
\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Metody/fig/Luenberg/luenberg_profil_h_noise.eps}
		\caption{Odhad Luenbergerova pozorovatele 1. profil s nejistotou měření rychlosti}
		\label{fig:Luenberger_noise_alpha}
	\end{subfigure}%
	~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Metody/fig/Luenberg/luenberg_profil_a_noise.eps}
		\caption{Odhad Luenbergerova pozorovatele 2. profil s nejistotou měření rychlosti}
		\label{fig:Luenberger_noise_velocity}
	\end{subfigure}%
	\caption{Luenbergerův pozorovatel: porovnání odhadu sklonu s reálným sklonem při měření se šumem}
	\label{fig:Luenberger_estimates_noise}
\end{figure}
\par
Pro 1. profil dosáhl Luenbergův pozorovatel hodnoty $\textup{RMSE} = 0.79$ a pro 2. profil $\textup{RMSE} = 0.75$.