import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn.functional as fn

def plot_function(x, y):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    # Move left y-axis and bottom x-axis to centre, passing through (0,0)
    ax.spines['left'].set_position('zero')
    ax.spines['bottom'].set_position('zero')

    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')

    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    plt.plot(x,y, lw=3)
    plt.show()


if __name__ == "__main__":
    x = torch.Tensor([-5, -0.0000001, 0, 0.0000001, 5])
    
    sign = torch.sign(x)
    sigmoid = 1/(1+torch.exp(-x))
    tanh = np.tanh(x)
    relu = fn.relu(x)
    lrelu = fn.leaky_relu(x, 0.1)
    elu = fn.elu(x)

    plot_function(x, sign)