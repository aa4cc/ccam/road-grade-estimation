\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\contentsline {paragraph}{Kl\'i\v cov\'a\ slova:}{vi}{paragraph*.1}%
\contentsline {paragraph}{Vedoucí:}{vi}{paragraph*.2}%
\babel@toc {english}{}\relax 
\contentsline {paragraph}{Keywords:}{vi}{paragraph*.3}%
\contentsline {paragraph}{Title\ translation:}{vi}{paragraph*.4}%
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {english}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\babel@toc {czech}{}\relax 
\contentsline {chapter}{\numberline {1}Úvod}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Cíl}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Motivace}{1}{section.1.2}%
\contentsline {section}{\numberline {1.3}Osnova}{2}{section.1.3}%
\contentsline {chapter}{\numberline {2}Související práce v oblasti odhadu sklonu vozovky}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}S GNSS}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}S barometrem a GNSS}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}S akcelerometrem a odhadem rychlosti z odometrie}{4}{section.2.3}%
\contentsline {section}{\numberline {2.4}S GNSS a více měřením ze stejné trasy}{4}{section.2.4}%
\contentsline {section}{\numberline {2.5}S výškovými údaji z map}{5}{section.2.5}%
\contentsline {section}{\numberline {2.6}Shrnutí nejvýznamějších metod}{5}{section.2.6}%
\contentsline {chapter}{\numberline {3}Model podélné dynamiky kolejového vozidla}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Matematický model kolejového vozidla}{7}{section.3.1}%
\contentsline {section}{\numberline {3.2}Profily dráhy}{8}{section.3.2}%
\contentsline {section}{\numberline {3.3}Model v prostředí \textit {Simulink}}{10}{section.3.3}%
\contentsline {section}{\numberline {3.4}Identifikace parametrů modelu}{10}{section.3.4}%
\contentsline {section}{\numberline {3.5}Simulační ověření}{13}{section.3.5}%
\contentsline {section}{\numberline {3.6}Přidání nejistot měření}{14}{section.3.6}%
\contentsline {chapter}{\numberline {4}Použité metody na odhad sklonu}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Luenbergerův pozorovatel}{15}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Linearizace modelu}{15}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Návrh pozorovatele}{16}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Výsledky}{18}{subsection.4.1.3}%
\contentsline {section}{\numberline {4.2}Diskrétní rozšířený (nelineární) Kalmanův filtr}{19}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Změna nezávislé jednotky}{19}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Návrh EKF}{20}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Výsledky}{21}{subsection.4.2.3}%
\contentsline {section}{\numberline {4.3}EKF s průměrováním více dat z jedné trasy}{22}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Implementace}{22}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Výsledky}{22}{subsection.4.3.2}%
\contentsline {chapter}{\numberline {5}Závěr}{25}{chapter.5}%
\contentsline {section}{\numberline {5.1}Shrnutí}{25}{section.5.1}%
\contentsline {section}{\numberline {5.2}Budoucí vývoj}{26}{section.5.2}%
\contentsline {chapter}{Bibliografie}{29}{chapter*.5}%
