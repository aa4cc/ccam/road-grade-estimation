import pandas as pd
import string
import sys
from datetime import datetime,timedelta,timezone
from csv import reader,writer
import os

base_name = "ad12627_reg_forcesp_1.aru"
file_name = base_name + ".xls"
new_file_name = base_name + ".csv"
print("Now replacing commas in file: " + file_name)

# replacing commas to dots in files
data_new = pd.read_csv(file_name, sep="\t", decimal=",")
data_new.to_csv(new_file_name, decimal=".", sep=";", index=False)

print("Replaced")
