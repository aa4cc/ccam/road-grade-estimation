%{
constants for Luenberger-observer simulink
height
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

%% constants for Luenberger-observer simulink
g = 9.81;
lambda_1 = -2;
lambda_2 = -3;
l_1 = lambda_1+lambda_2;
l_2 = lambda_1*lambda_2/g;
A_L = [0 -g; 0 0]; 
B_L = [1/m 0; 0 0];
C_L = [1 0]; 
D_L = [0 0]; 
eigen_L = [lambda_1 lambda_2]; 
initial_L = [l_1 0;l_2 0];

% 0.06666
x_s = out.x_s.signals.values;
alpha_1 = out.alpha_hat.signals(1).values;
alpha_2 = out.alpha_hat.signals(2).values;

v_1 = out.v_hat.signals(1).values;
v_2 = out.v_hat.signals(2).values;

final_idx = 10000;

figure("Name","Alfa");
plot(x_s(1:final_idx),alpha_1(1:final_idx),"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]');
ylabel('sklon [°]');
plot(x_s(1:final_idx),alpha_2(1:final_idx),"LineWidth",2,"LineStyle","--","Color","red");
legend('Pozorovatel','Model');
hold off;

figure("Name","Rychlost");
plot(x_s(1:final_idx),v_1(1:final_idx),"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]');
ylabel('rychlost [m/s]');
plot(x_s(1:final_idx),v_2(1:final_idx),"LineWidth",2,"Color","red","LineStyle","--");
legend('Pozorovatel','Model');
hold off;