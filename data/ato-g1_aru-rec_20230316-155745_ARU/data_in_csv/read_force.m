%{
data processing of Skoda Transportaion data from electric train 16Ev
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

%% loading data
dat_force_m = readtable("ad12627_reg_forcesp_1.aru.csv", "ExpectedNumVariables", 4);
dat_force_slope = readtable("ad12627_reg_slopefor_1.aru.csv", "ExpectedNumVariables", 4);
dat_accel = readtable("ad12627_odo_acceler_1.aru.csv", "ExpectedNumVariables", 4);
dat_speed = readtable("ad12627_odo_speed_1.aru.csv", "ExpectedNumVariables", 4);
dat_speedrq = readtable("ad12627_reg_speedrq_1.aru.csv", "ExpectedNumVariables", 4);

end_index = 2000;
x_classic = table2array(dat_force_m(1:end_index,2));
x_slope = table2array(dat_force_slope(:,2));

F_m = table2array(dat_force_m(1:end_index,3));
F_s = table2array(dat_force_slope(:,3));
accel = table2array(dat_accel(1:end_index,3));
speed = table2array(dat_speed(1:end_index,3));
speed_rq = table2array(dat_speedrq(1:end_index,3));

%% slope alpha 
alpha = asind(F_s./(m*g));


%% comparasion of simulated and real data
start_offset = 75394.2;
x_s = out.x_s.signals.values;
x_classic = x_classic - start_offset;

F_m_s = out.F_m_s.signals.values;
v_s = out.v_s.signals.values;
%alpha_s = asind(out.sin_a.signals.values);
a_s = out.a_s.signals.values;
t_s = out.F_m_s.time;

number_of_elements = length(x_s);

F_m_q = interp1(x_s,F_m_s,x_classic);
v_q = interp1(x_s,v_s,x_classic);
%alpha_q = interp1(x_s,alpha_s,x_classic);
a_q = interp1(x_s,a_s,x_classic);

t_q = 1:1:2000;
x_classic_q = interp1(t_q,x_classic,t_s);

end_index = 0;
figure("Name","Síla motoru");
plot(x_classic,F_m,"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic,F_m_q,"LineWidth",2,"LineStyle","--","Color","red");
legend('Simulovaná data','Naměřená data');
xlabel('s [m]');
ylabel('F_m [N]');
hold off;

figure("Name","Rychlost");
plot(x_classic,speed,"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic,v_q,"LineWidth",2,"Color","red","LineStyle","--");
legend('Simulovaná data','Naměřená data');
xlabel('s [m]');
ylabel('v [m/s]');
hold off;

figure("Name","Zrychlení");
plot(x_classic,accel,"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic,a_q,"LineWidth",2,"Color","red","LineStyle","--");
legend('Simulovaná data','Naměřená data');
xlabel('s [m]');
ylabel('a [m/s^2]');
hold off;

% figure("Name","úhel sklonu");
% plot(x_classic,alpha_q,"LineWidth",2,"LineStyle","--","Color","red");
% hold on;
% grid on;
% plot(x_slope,alpha,"LineWidth",2,"Color","blue");
% legend('Simulovaná data','Naměřená data');
% xlabel('index [~]');
% ylabel('s [m]');
% hold off;


