forcesp:
	Archive name:	ad12627_reg_forcesp_1.aru
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)
	Variable:	
	Primary key:	110204
	Address:	0x0000
	VarIndex:	0
	Data type name:	
	Data type:	16
	Byte length:	4
	Representation:	
	Repres.code:	0
	Physical unit:	
	Multiplier:	1
	Maximum records:	15000
	Number of records:	2424
	Next record index:	2424
	Creation year:	2023
	First record X value:	75394,2
	Last record X value:	76801,86
	Comment:	

gps_lat
	Archive name:	ad12627_gps_lat_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	10086179		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	17		
	Byte length:	8		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	931		
	Next record index:	931		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,64		
	Comment:			

gps_lon
	Archive name:	ad12627_gps_lon_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	10086180		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	17		
	Byte length:	8		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	931		
	Next record index:	931		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,64		
	Comment:			

acceler
	Archive name:	ad12627_odo_acceler_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	79591		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	16		
	Byte length:	4		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	2424		
	Next record index:	2424		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,86		
	Comment:			

speed
	Archive name:	ad12627_odo_speed_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	79592		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	16		
	Byte length:	4		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	2424		
	Next record index:	2424		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,86		
	Comment:			

relforce
	Archive name:	ad12627_reg_relforce_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	130094		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	16		
	Byte length:	4		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	2424		
	Next record index:	2424		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,86		
	Comment:			

slopefor
	Archive name:	ad12627_reg_slopefor_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	4942697		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	16		
	Byte length:	4		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	931		
	Next record index:	931		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,64		
	Comment:			

speedrq
	Archive name:	ad12627_reg_speedrq_1.aru		
	Archive version:	0x82 (XY archive, Little Endian, variable rec.length)		
	Variable:			
	Primary key:	243433		
	Address:	0x0000		
	VarIndex:	0		
	Data type name:			
	Data type:	16		
	Byte length:	4		
	Representation:			
	Repres.code:	0		
	Physical unit:			
	Multiplier:	1		
	Maximum records:	15000		
	Number of records:	2424		
	Next record index:	2424		
	Creation year:	2023		
	First record X value:	75394,2		
	Last record X value:	76801,86		
	Comment:			
