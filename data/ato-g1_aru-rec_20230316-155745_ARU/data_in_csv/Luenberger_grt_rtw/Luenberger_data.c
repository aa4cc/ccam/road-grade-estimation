/*
 * Luenberger_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Luenberger".
 *
 * Model version              : 1.104
 * Simulink Coder version : 9.6 (R2021b) 14-May-2021
 * C source code generated on : Sun Apr  2 17:07:53 2023
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Luenberger.h"
#include "Luenberger_private.h"

/* Block parameters (default storage) */
P_Luenberger_T Luenberger_P = {
  /* Variable: initial_L
   * Referenced by: '<S1>/Unit Delay'
   */
  { 0.0, 0.0 },

  /* Expression: Ad
   * Referenced by: '<S1>/Ad matrix'
   */
  { 1.0, 0.0, -0.009810000000000001, 1.0 },

  /* Expression: Bd
   * Referenced by: '<S1>/Bd matrix'
   */
  { 0.0, 0.0, 0.0, 0.0 },

  /* Expression: Cd
   * Referenced by: '<S1>/Cd matrix'
   */
  { 1.0, 0.0 },

  /* Expression: Dd
   * Referenced by: '<S1>/Dd matrix'
   */
  { 0.0, 0.0 },

  /* Expression: Ld
   * Referenced by: '<S1>/Ld matrix'
   */
  { 0.0089795314633262, -0.0020295857502422416 }
};
