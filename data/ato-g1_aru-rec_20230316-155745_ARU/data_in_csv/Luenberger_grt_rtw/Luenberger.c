/*
 * Luenberger.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Luenberger".
 *
 * Model version              : 1.104
 * Simulink Coder version : 9.6 (R2021b) 14-May-2021
 * C source code generated on : Sun Apr  2 17:07:53 2023
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Luenberger.h"
#include "Luenberger_private.h"

/* Block states (default storage) */
DW_Luenberger_T Luenberger_DW;

/* External inputs (root inport signals with default storage) */
ExtU_Luenberger_T Luenberger_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_Luenberger_T Luenberger_Y;

/* Real-time model */
static RT_MODEL_Luenberger_T Luenberger_M_;
RT_MODEL_Luenberger_T *const Luenberger_M = &Luenberger_M_;

/* Model step function */
void Luenberger_step(void)
{
  real_T rtb_Sum2;
  real_T tmp;
  real_T tmp_0;

  /* Outport: '<Root>/xhat' incorporates:
   *  UnitDelay: '<S1>/Unit Delay'
   */
  Luenberger_Y.xhat[0] = Luenberger_DW.UnitDelay_DSTATE[0];
  Luenberger_Y.xhat[1] = Luenberger_DW.UnitDelay_DSTATE[1];

  /* Sum: '<S1>/Sum2' incorporates:
   *  Gain: '<S1>/Cd matrix'
   *  Gain: '<S1>/Dd matrix'
   *  Inport: '<Root>/u'
   *  Inport: '<Root>/y'
   *  Sum: '<S1>/Sum'
   *  UnitDelay: '<S1>/Unit Delay'
   */
  rtb_Sum2 = Luenberger_U.v - ((Luenberger_P.Ddmatrix_Gain[0] * Luenberger_U.u[0]
    + Luenberger_P.Ddmatrix_Gain[1] * Luenberger_U.u[1]) +
    (Luenberger_P.Cdmatrix_Gain[0] * Luenberger_DW.UnitDelay_DSTATE[0] +
     Luenberger_P.Cdmatrix_Gain[1] * Luenberger_DW.UnitDelay_DSTATE[1]));

  /* Gain: '<S1>/Ad matrix' incorporates:
   *  UnitDelay: '<S1>/Unit Delay'
   */
  tmp = Luenberger_P.Admatrix_Gain[0] * Luenberger_DW.UnitDelay_DSTATE[0] +
    Luenberger_DW.UnitDelay_DSTATE[1] * Luenberger_P.Admatrix_Gain[2];
  tmp_0 = Luenberger_DW.UnitDelay_DSTATE[0] * Luenberger_P.Admatrix_Gain[1] +
    Luenberger_DW.UnitDelay_DSTATE[1] * Luenberger_P.Admatrix_Gain[3];

  /* Update for UnitDelay: '<S1>/Unit Delay' incorporates:
   *  Gain: '<S1>/Bd matrix'
   *  Gain: '<S1>/Ld matrix'
   *  Inport: '<Root>/u'
   *  Sum: '<S1>/Sum1'
   *  Sum: '<S1>/Sum3'
   */
  Luenberger_DW.UnitDelay_DSTATE[0] = ((Luenberger_P.Bdmatrix_Gain[0] *
    Luenberger_U.u[0] + Luenberger_U.u[1] * Luenberger_P.Bdmatrix_Gain[2]) +
    Luenberger_P.Ldmatrix_Gain[0] * rtb_Sum2) + tmp;
  Luenberger_DW.UnitDelay_DSTATE[1] = ((Luenberger_U.u[0] *
    Luenberger_P.Bdmatrix_Gain[1] + Luenberger_U.u[1] *
    Luenberger_P.Bdmatrix_Gain[3]) + Luenberger_P.Ldmatrix_Gain[1] * rtb_Sum2) +
    tmp_0;

  /* Matfile logging */
  rt_UpdateTXYLogVars(Luenberger_M->rtwLogInfo, (&Luenberger_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.001s, 0.0s] */
    if ((rtmGetTFinal(Luenberger_M)!=-1) &&
        !((rtmGetTFinal(Luenberger_M)-Luenberger_M->Timing.taskTime0) >
          Luenberger_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(Luenberger_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Luenberger_M->Timing.clockTick0)) {
    ++Luenberger_M->Timing.clockTickH0;
  }

  Luenberger_M->Timing.taskTime0 = Luenberger_M->Timing.clockTick0 *
    Luenberger_M->Timing.stepSize0 + Luenberger_M->Timing.clockTickH0 *
    Luenberger_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void Luenberger_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Luenberger_M, 0,
                sizeof(RT_MODEL_Luenberger_T));
  rtmSetTFinal(Luenberger_M, 45.0);
  Luenberger_M->Timing.stepSize0 = 0.001;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = (NULL);
    Luenberger_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(Luenberger_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(Luenberger_M->rtwLogInfo, (NULL));
    rtliSetLogT(Luenberger_M->rtwLogInfo, "tout");
    rtliSetLogX(Luenberger_M->rtwLogInfo, "");
    rtliSetLogXFinal(Luenberger_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(Luenberger_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(Luenberger_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(Luenberger_M->rtwLogInfo, 0);
    rtliSetLogDecimation(Luenberger_M->rtwLogInfo, 1);
    rtliSetLogY(Luenberger_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(Luenberger_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(Luenberger_M->rtwLogInfo, (NULL));
  }

  /* states (dwork) */
  (void) memset((void *)&Luenberger_DW, 0,
                sizeof(DW_Luenberger_T));

  /* external inputs */
  (void)memset(&Luenberger_U, 0, sizeof(ExtU_Luenberger_T));

  /* external outputs */
  (void)memset(&Luenberger_Y, 0, sizeof(ExtY_Luenberger_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(Luenberger_M->rtwLogInfo, 0.0, rtmGetTFinal
    (Luenberger_M), Luenberger_M->Timing.stepSize0, (&rtmGetErrorStatus
    (Luenberger_M)));

  /* InitializeConditions for UnitDelay: '<S1>/Unit Delay' */
  Luenberger_DW.UnitDelay_DSTATE[0] = Luenberger_P.initial_L[0];
  Luenberger_DW.UnitDelay_DSTATE[1] = Luenberger_P.initial_L[1];
}

/* Model terminate function */
void Luenberger_terminate(void)
{
  /* (no terminate code required) */
}
