/*
 * Luenberger.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Luenberger".
 *
 * Model version              : 1.104
 * Simulink Coder version : 9.6 (R2021b) 14-May-2021
 * C source code generated on : Sun Apr  2 17:07:53 2023
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Luenberger_h_
#define RTW_HEADER_Luenberger_h_
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef Luenberger_COMMON_INCLUDES_
#define Luenberger_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* Luenberger_COMMON_INCLUDES_ */

#include "Luenberger_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                (&(rtm)->Timing.taskTime0)
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T UnitDelay_DSTATE[2];          /* '<S1>/Unit Delay' */
} DW_Luenberger_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T u[2];                         /* '<Root>/u' */
  real_T v;                            /* '<Root>/y' */
} ExtU_Luenberger_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T xhat[2];                      /* '<Root>/xhat' */
} ExtY_Luenberger_T;

/* Parameters (default storage) */
struct P_Luenberger_T_ {
  real_T initial_L[2];                 /* Variable: initial_L
                                        * Referenced by: '<S1>/Unit Delay'
                                        */
  real_T Admatrix_Gain[4];             /* Expression: Ad
                                        * Referenced by: '<S1>/Ad matrix'
                                        */
  real_T Bdmatrix_Gain[4];             /* Expression: Bd
                                        * Referenced by: '<S1>/Bd matrix'
                                        */
  real_T Cdmatrix_Gain[2];             /* Expression: Cd
                                        * Referenced by: '<S1>/Cd matrix'
                                        */
  real_T Ddmatrix_Gain[2];             /* Expression: Dd
                                        * Referenced by: '<S1>/Dd matrix'
                                        */
  real_T Ldmatrix_Gain[2];             /* Expression: Ld
                                        * Referenced by: '<S1>/Ld matrix'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Luenberger_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_Luenberger_T Luenberger_P;

/* Block states (default storage) */
extern DW_Luenberger_T Luenberger_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_Luenberger_T Luenberger_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Luenberger_T Luenberger_Y;

/* Model entry point functions */
extern void Luenberger_initialize(void);
extern void Luenberger_step(void);
extern void Luenberger_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Luenberger_T *const Luenberger_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('model_of_tram/Luenberger Observer')    - opens subsystem model_of_tram/Luenberger Observer
 * hilite_system('model_of_tram/Luenberger Observer/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'model_of_tram'
 * '<S1>'   : 'model_of_tram/Luenberger Observer'
 */
#endif                                 /* RTW_HEADER_Luenberger_h_ */
