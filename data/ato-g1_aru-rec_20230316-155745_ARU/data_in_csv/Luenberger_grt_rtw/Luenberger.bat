
set MATLAB=C:\Users\radac\Apps\Matlab 2021b

cd .

if "%1"=="" ("C:\Users\radac\Apps\MATLAB~1\bin\win64\gmake"  -f Luenberger.mk all) else ("C:\Users\radac\Apps\MATLAB~1\bin\win64\gmake"  -f Luenberger.mk %1)
@if errorlevel 1 goto error_exit

exit /B 0

:error_exit
echo The make command returned an error of %errorlevel%
exit /B 1