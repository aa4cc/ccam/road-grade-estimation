%{
constants for model
height
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clc;clear;

%% constants for simulink model of vehicle
g = 9.81;
m = 150000;
c_w = 1.05;
A_a = 14.4840;
c_r = 0.0005;
% nadmorska vyska = 273m, T = 10C 
rho_a = 1.201;

%% constants for Luenberger-observer simulink
lambda_1 = -200;
lambda_2 = -300;
l_1 = lambda_1+lambda_2;
l_2 = lambda_1*lambda_2/g;
A_L = [0 -g; 0 0]; 
B_L = [1/m 0; 0 0];
C_L = [1 0]; 
D_L = [0 0]; 
eigen_L = [lambda_1 lambda_2]; 
L = [1;-5];


%% profil trati 
d = linspace(0,6000,60000);
z = 20*sin(1/300*d);
alpha = 20*1/300*cos(1/300*d);

% ackermanuv vzorec - place 
% poly meho systemu hlavne pol rychlosti -> chci vlastni cisla systemu byly
% 10x vedle