%{
use height profile from Jakub Kašpar
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

%heights
load("vyskovy_profil_kuba.mat");

%altitudes
load("altitudesFilteredTram3.mat");

n_h = length(heights);
n_a = length(altitudes);

ds = 2;
x_s_h = 0:ds:(n_h-1)*ds;
x_s_a = 0:ds:(n_a-1)*ds;

%% profil trati od Kuby
d_h = linspace(0,6000,(n_h-1)*ds);
z_h = interp1(x_s_h,heights,d_h)*0.5;

d_a = linspace(0,60000,(n_a-1)*ds);
z_a = interp1(x_s_a,altitudes,d_a);

alpha_h = [0,diff(z_h)];
alpha_filt = medfilt1(alpha_h,5);

alpha_a = [0,diff(z_a)];
