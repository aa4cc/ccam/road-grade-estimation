Datasets are separated into 2 categories:

1) emu_DD_MM_HH_NN
where DD - day, MM- month, HH- hour, NN - minutes. 
Here are saved data from are IMU block. format of saved data: 
X_GYRO , Y_GYRO, Z-GYRO , X_ACCEL, Y-ACCEL, Z-ACCEL

2) ev3_DD_MM_HH_NN_c
c - our gear speed we went by: 
SPEED_MAX = 50 * gear [mm/s]
format:
time, 	    angle_left,           angle_right,            angle_steer,           control_lines_data_color
(timestamp),(angle of left motor),(angle of rightmotor), (angle of steer motor), (color of detected control line)