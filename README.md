# Road and railroad grade estimation using onboard measurement and computation

The **goal** of this project is to develop an automated procedure for estimation of the grade (the slope) of the (rail)road using onboard measurements and computation.

The **motivation** for such work is that an accurate model of the (rail)road grade is a prerequisite for model-based solutions to several vehicle control problems such as collision avoidance and energy efficient speed planning and control.

There are two directions of attack in our project:

- First, using the experimental data obtained using a dedicated instrumentation on a dedicated vehicle during a **dedicated experiment**.
- Second, using the **operational data** obtained using a conventional instrumentation available onboard an arbitrary vehicle in a normal operation.

An advantage of the **dedicated experiment** case is that the instrumentation can be finetuned (and extra paid) right for this purpose. Similarly the actual ride can be affected (perhaps even commanding the driver to stop here and there). A disadvantage is that such experiment cannnot be repeated many times and at all places.  

A disadvantage of the **operational data** case is that the quality of the onboard instrumentation need not be as high and the ride will not be particularly tailored to the needs of the estimation procedure. An advantage is that the scalability – the amount of data obtained this way could be essentially unlimited – possibly from all the vehicles in the fleet, from many rides on the same (rail)road, and possibly for all (rail)roads in the area. 

The two approaches will certainly share some ideas but still they might call for different methods. Ultimately the two will support each other. In particular, the results of the procedure for the dedicated experiment might provide calibration date for the operational-data based procedure.

The (initial idea) of the key instrumentation is
- IMU. In particular a three axis accelerometer. Whether gyros can be of some use too is hard to tell at first.
- Onboard measurement of traction torque/force. The electric current through the motor(s) for trams.

The elevation data available in a **digital map** of the terrain is also planned to be used, alghouth it is already known (to us) that the grade (slope) estimation based merely on the digital map is rather poor. 

In fact, the digital map can eventually be a receiver of the estimated grades, if there are ways ho to insert such information into the existing data structures, otherwise new data structures will have to be designed and stored outside the map.

# Finished work 
In this section it is summarized the acomplished work on this theme.

## Bachelor project
Submitted and graded bachelor thesis can be found on link: https://dspace.cvut.cz/handle/10467/109424. 
As part of bachelor thesis I implemented model of tram for data generation and model-based filtration. 
Then I implemented 3 methods for road grade estimation. 

### Model
For model I used physical model with forces shown:

<img title="Podélné síly působící na kolejové vozidlo" alt="Alt text" src="/reports/figures/tram.png">
The model was implemented in Simulink as:
<img title="Výsledné simulinkové schéma pro podélný pohyb kolejového vozidla" alt="Alt text" src="/reports/figures/syst.png">

### Luenberger
Luenberger observer followed this struction:
<img title="Struktura Luenbergerova pozorovatele" alt="Alt text" src="/reports/figures/luenberg.png">

### EKF and EKF with averaging
EKF followed this struction:
![Struktura EKF s průměrováním více dat z jedné trasy](reports/figures/scheme_EKF_average.png)

### Conclusion
Here we can see all implemented observers that were following 2 altitude profiles.
![Porovnání odhadu EKF průměrování 1.profilu](reports/figures/all_profil_a.png)
![Porovnání odhadu EKF průměrování 2.profilu](reports/figures/all_profil_h.png)

# Current Development 
Currently mainly focusing on implementing methods from bachelor thesis to work with real data from tram.

## Process real data 
Data was measured on tram number 7 in Prague with colaboration with Škoda digital. Data measured can be seen on repository: [Real measurements from tram](https://gitlab.fel.cvut.cz/aa4cc/ccam/skodata.git).

I used program seen also in this repositary : [road_grade_data](src/road_grade_data.py) to get file with significant measurements for my use.

## Map data
Firstly I needed to get exact locations of tram railway.
I decied to use [OSM](https://www.openstreetmap.org/#map=8/49.817/15.478) data as ground truth for this data. Then I created program that gets latitude and longitude from OSM based on relation id with our tram railway. [road_grade_data](src/gps_OSM.py)
Data can be seen:
![Map of specific relation (tram railway)](reports/figures/map_relation.png)
Then it was needed to merge data with its altitudes. I used altitude map from ![GEOPORTAL](https://www.geoportalpraha.cz/cs/data/metadata/6F72EDDF-CAA4-4243-8776-7006CB0B2521). This altitude map is visualized on: 
![Altitude map Prague](reports/figures/prague_height_map.png)

Then I needed to merge data from tram with gps locations. I searched in tram data for nearest locations relative to gps from OSM.
I unconsciously used method similar to methods developted under the name : **MapMatching**.
I used k-nearest neighbors as seen: [road_grade_data](src/merge_tram_OSM_kNn.py).

### Model automatically
Then I wanted to automate process of finding constants from model of tram. So I developed nonlinear optimalization algorithm using lsqnonlin.
This algorithm can be seen: [model_fit](src/model_fit_lsqnonlin.m).
I put boundaries on searched parameters (the weight of tram, constant for air resistance, constant for rolling resistance).
And program after 26. iteration: 
![Iterations](reports/figures/converted_minimum.png)
found minimum for parameters (m = 50000, K = 21.242, c_r = 0.0062).

### EKF
Then I altered EKF algorithm from bachelor thesis to be applied on real data. Then I decided to pinpoint specific locations that are different in model and altitude map from GEOPORTAL. This was developed with motivation of correct GEOPORTAL data, that can be a bit problematic when on bridge or tunnel.
Result can be seen:
![EKF results with geoportal data](reports/figures/altitude.png)
Then I visualized suspicious places on map:
![Map of suspicious places with regard on altitude map and model](reports/figures/map_weird.png)

# TODO
