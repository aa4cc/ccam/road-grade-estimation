import pandas as pd
import sys
from datetime import datetime,timedelta,timezone
from csv import reader,writer
import os 


file_name = sys.argv[1]
data_new = pd.read_csv(file_name,sep=";",decimal=",")

new_file_name = file_name + "_dots.csv"
data_new.to_csv(new_file_name,decimal=".")