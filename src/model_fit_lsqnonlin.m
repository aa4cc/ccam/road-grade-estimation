% Název: model_fit_lsqnonlin.m
% Popis: Finds constants for model of tram based on loaded data using
% nonlinear optimalization
% Autor: Radek Chládek
% Datum: 21.09.2023
% Verze: 1.0
% Copyright (c) 2023 Radek Chládek
% 
% Tento program je svobodný software: můžete ho šířit a upravovat podle
% Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
% verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.
% 
% Tento program je distribuován v naději, že bude užitečný,
% ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
% PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
% Svobodné licenci GNU.
% 
% Tuto licenci jste měli obdržet spolu s tímto programem.
% Pokud ne, podívejte se na http://www.gnu.org/licenses/.

% Latitude,Longitude,Altitude_dsm,Altitude_dtm,closest_latitude,closest_longitude,speed,force_actual,mass,accel,Road_grade
data_tram = readmatrix("merged_grade.csv");

start_value = 1; %400
end_value = 641;
% Latitude,Longitude,Altitude_dsm,Altitude_dtm,closest_latitude,closest_longitude,speed,force_actual,mass,accel,Road_grade
v = double(data_tram(start_value:end_value,7)); % vektor derivace rychlosti
F_m = double(data_tram(start_value:end_value,8).*1000); % vektor sil kN*1000 -> N
alpha = double(data_tram(start_value:end_value,11)); % uhel
accel = double(data_tram(start_value:end_value,10)); % acceleration
z = double(data_tram(start_value:end_value,3)); 

c_w = 1.05;
A_a = 14.4840;
% nadmorska vyska = 273m, T = 10C 
rho_a = 1.201;

A_guess = 1/2*c_w*A_a*rho_a;
m_guess = 42000;
c_r_guess = 0.0005;
initial_params = [A_guess, m_guess, c_r_guess];

options = optimoptions('lsqnonlin', 'Display', 'iter','PlotFcns','optimplotfval');
lb = [0,40000,0];
ub = [inf,50000,inf];
params_optimal = lsqnonlin(@(params) myObjectiveFunction(params, v, F_m, alpha, accel), initial_params, lb, ub, options);

% Získání výsledných hodnot konstant
K = params_optimal(1);
m = params_optimal(2);
g = 9.81;
c_r = params_optimal(3);

display(initial_params);
display(params_optimal);

function residuals = myObjectiveFunction(params, v, F_m, alpha, accel)
    A = params(1);
    m = params(2);
    c_r = params(3);
    g = 9.81;

    % Vypočet akcelerace z vaší rovnice
    dvdt_model = (F_m - m * g * sin(alpha) - A * v.^2 - sign(v) * (m * g * c_r)) / m;

    % Výpočet rozdílu mezi naměřenou a vypočtenou akcelerací
    residuals = accel - dvdt_model;
end
