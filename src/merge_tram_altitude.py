"""
Název: merge_tram_altitude
Popis: Merges csv files from tram with altitude from geoportal
Autor: Radek Chládek
Datum: 21.09.2023
Verze: 1.0
Copyright (c) 2023 Radek Chládek

Tento program je svobodný software: můžete ho šířit a upravovat podle
Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.

Tento program je distribuován v naději, že bude užitečný,
ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
Svobodné licenci GNU.

Tuto licenci jste měli obdržet spolu s tímto programem.
Pokud ne, podívejte se na http://www.gnu.org/licenses/.
"""

import pandas as pd
from geopy.distance import great_circle

# first csv file
df1 = pd.read_csv('gps_altitude_tram_7.csv')

# second csv file
df2 = pd.read_csv('tram_7_data.csv')

# empty dataset
result_df = pd.DataFrame()

# iter through 1
for index1, row1 in df1.iterrows():
    coord1 = (row1['latitude'], row1['longitude'])
    closest_distance = float('inf')
    closest_row = None

    # iter through 2
    for index2, row2 in df2.iterrows():
        coord2 = (row2['latitude'], row2['longitude'])
        distance = great_circle(coord1, coord2).kilometers

        if distance < closest_distance:
            closest_distance = distance
            closest_row = row2

    # Přidejte sloupec s vzdáleností k nejbližšímu bodu 
    row1['closest_distance'] = closest_distance

    # Přidejte sloupec s informacemi z nejbližšího bodu
    for column in df2.columns:
        row1[f'closest_{column}'] = closest_row[column]

    # Přidejte řádek do výsledného DataFrame
    result_df = result_df.append(row1, ignore_index=True)

# Uložte výsledný DataFrame do CSV souboru
result_df.to_csv('spojeny_soubor.csv', index=False)
