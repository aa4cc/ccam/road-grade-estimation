%{
RMSE values comparison
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clc,clear;

alpha_q_1 = struct2array(load("ground_truth_3400_h.mat"));
database_1 = struct2array(load("iteration_400_v_5_t_10_3400.mat"));

alpha_q_2 = struct2array(load("ground_truth_20000_a.mat"));
database_2 = struct2array(load("iteration_150_v_5_t_10_20000.mat"));

iterations = 1:1:150;

rmse_values_1 = NaN(1,length(iterations));
rmse_values_2 = NaN(1,length(iterations));

rmse_values_1(1,1) = rmse(alpha_q_1,database_1(1,:));
rmse_values_2(1,1) = rmse(alpha_q_2,database_2(1,:));

for i = 2:length(rmse_values_1)
    rmse_values_1(1,i) = rmse(alpha_q_1,sum(database_1(1:iterations(i),:))/iterations(i));
    rmse_values_2(1,i) = rmse(alpha_q_2,sum(database_2(1:iterations(i),:))/iterations(i));
end

figure("Name","RMSE hodnoty závislé na počtu hodnot");
plot(iterations,rmse_values_1,"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(iterations,rmse_values_2,"LineWidth",2,"Color","red","LineStyle","--");
xlabel('počet průměrovaných iterací [~]');
ylabel('RMSE [~]');
legend("1. profil", "2.profil");
hold off;