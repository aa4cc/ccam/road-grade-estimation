clc;clear;

%load the dataset
dat = readtable("emu16_08_13_30.csv", "ExpectedNumVariables", 6);

%Output Noise No filtering 0.6 mg rms
%default:0.00019247
%accel_noise_rms = (0.6*10^(-3)*9.8)^2; 
accel_noise_rms = (0.6*10^(-3)*9.8)^2; 

%ADIS16465-1, 1 σ, no filtering, x-axis 0.05 °/sec rms
%ADIS16465-1, 1 σ, no filtering, y-axis and z-axis 0.07 °/sec rms
%default:9.1385e-5 
gyro_noise_rms = (deg2rad(0.06))^2; 

%In-Run Bias Stability ADIS16465-1, 1 σ 2 °/hr 
%default:3.0462e-13 
gyro_drift_noise = (deg2rad(2/3600))^2;

%Best fit straight line, ±2 g 0.25 % FS
%Best fit straight line, ±8 g, x-axis 0.5 % FS
%Best fit straight line, ±8 g, y-axis and z-axis 1.5 % FS
%default: 0.5
linear_accel_decay = 0.5;

%not in datasheet should have units:(m/s2)2
%default: 0.0096236
linear_accel_noise = 0.0096236;

%dat processing
x_gyro = deg2rad(table2array(dat(:,1)));
y_gyro = deg2rad(table2array(dat(:,2)));
z_gyro = deg2rad(table2array(dat(:,3)));
x_accel = table2array(dat(:,4));
y_accel = table2array(dat(:,5));
z_accel = table2array(dat(:,6));
accelerometerReadings = [x_accel y_accel z_accel];
gyroscopeReadings = [x_gyro y_gyro z_gyro];

%decimal accuracy
decim = 5;
%frequency of IMU
Fs = 2000;

fuse = imufilter('ReferenceFrame','ENU','SampleRate',Fs,'DecimationFactor',decim,'AccelerometerNoise',accel_noise_rms,'GyroscopeNoise',gyro_noise_rms,'GyroscopeDriftNoise',gyro_drift_noise,'LinearAccelerationDecayFactor',linear_accel_decay,'LinearAccelerationNoise',linear_accel_noise);
q = fuse(accelerometerReadings,gyroscopeReadings);

time = (0:decim:size(accelerometerReadings,1)-1)/Fs;

figure(2)
orientations = eulerd(q,'XYZ','frame');
plot(time,orientations(:,2),'LineWidth',2)
grid on
title('Orientation Estimate')
legend('Z-axis','Y-axis','X-axis')
xlabel('Time (s)')
ylabel('Rotation (degrees)')


