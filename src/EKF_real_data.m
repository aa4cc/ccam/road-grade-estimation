%{
Implementation of Extended Kalman Filter
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2024
%}

rng(10);

font_size = 14;
line_width = 1.5;

% Latitude,Longitude,Altitude_dsm,Altitude_dtm,closest_latitude,closest_longitude,speed,force_actual,mass,accel,Road_grade
data_tram = readmatrix("merged_grade.csv");

% Latitude,Longitude,Altitude_dsm,Altitude_dtm,closest_latitude,closest_longitude,speed,force_actual,mass,accel,Road_grade
v = transpose(data_tram(:,7)); % vektor derivace rychlosti
F_m = transpose(data_tram(:,8)).*1000; % vektor sil
alpha = transpose(data_tram(:,11)); % uhel
accel = transpose(data_tram(:,10)); % acceleration
z = transpose(data_tram(:,3)); 
%alpha = [0, diff(z)];

% saving all the measurements into one array 
measurements = [v; z];

%% EKF 

% initialize EKF
% pro h
trueInitialState = [v(1);z(1);alpha(1)]; % [v,z,alpha]

initialCovariance = diag([1,0.001,0.001]);
processNoise = diag([1; 1; 1]); % Process noise matrix
measureNoise = diag([100;200]); % Measurement noise matrix. Units are m^2 and rad^2.

filter = trackingEKF(State=trueInitialState,StateCovariance=initialCovariance, ...
    StateTransitionFcn=@stateModel,ProcessNoise=processNoise, ...
    MeasurementFcn=@measureModel,MeasurementNoise=measureNoise);
estimateStates(:,1) = filter.State;
ds = 1;

% Najděte indexy záporných hodnot
negative_indices = find(F_m < 0);
y_n_i = zeros(size(negative_indices));

% Running of EKF
for i=2:length(v)
    predict(filter,ds,F_m(:,i),m,K,c_r);
    estimateStates(:,i) = correct(filter,measurements(:,i));
end

figure("Name","Alfa");
plot(estimateStates(3,:),"LineWidth",line_width,"Color","red");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
plot((alpha),"LineWidth",2,"Color","blue");
scatter(negative_indices,estimateStates(3,negative_indices),'o','filled');
legend('Pozorovatel','Naměřená data','Úseky s bržděním','FontSize',font_size);
hold off;

figure("Name","Rychlost");
plot(v,"LineWidth",line_width,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
plot(estimateStates(1,:),"LineWidth",line_width ,"Color","red");
scatter(negative_indices,estimateStates(1,negative_indices),'o','filled');
legend('Naměřená data','Pozorovatel','Úseky s bržděním','FontSize',font_size);
hold off;

differences = abs(estimateStates(2,:)-z);

max_difference = 5;

idx = find(differences >= max_difference);

suspicious_place = estimateStates(2,idx);

figure("Name","Výška");
plot(z,"LineWidth",line_width ,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('výška [m]','FontSize',font_size);
plot(estimateStates(2,:),"LineWidth",line_width,"Color","red");
scatter(negative_indices,estimateStates(2,negative_indices),'o','filled');
scatter(idx,estimateStates(2,idx),'o','filled');
legend('Naměřená data','Pozorovatel','Úseky s bržděním','Divné úseky','FontSize',font_size);
hold off;

lat = transpose(data_tram(idx,1));
lon = transpose(data_tram(idx,2));
T = table(lat', lon', 'VariableNames', {'Latitude', 'Longitude'});

writetable(T,'weird_places.csv');

%% State model of tram without process noise
function stateNext = stateModel(state,ds,F_m_q,m,K,c_r)
    g = 9.81;
    % nadmorska vyska = 273m, T = 10C 
    dvds = F_m_q/(m*state(1))-((1/(2*m))*(K*state(1)))-(g/state(1))*(c_r+sin(state(3)));
    v_k = state(1) + ds*dvds;
    z_k = state(2) + ds*sin(state(3)) ;
    a_k = state(3);
    stateNext = [v_k;z_k;a_k];
end

%% output of model 
function z = measureModel(state)
    H = [1 0 0; 
         0 1 0];    
    z = H*state;
end

% State transition Jacobian:
function jacobian = jacobState(state,ds,F_m_q,m,K,c_r)
    % Validate that state is 4-elements long and that there are two inputs
    g = 9.81;
    jacobian = [1+ds*(-F_m_q/(m*state(1)^2)+((c_r+sin(state(3))*g)/state(1)^2)-((1/2*K)/m)) 0 -(ds*g*cos(state(3)))/state(1); 
           0 1 ds*cos(state(3)); 
           0 0 1];
end

