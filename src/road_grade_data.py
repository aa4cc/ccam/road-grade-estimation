"""
Název: merge_tram_altitude
Popis: Merges csv files from tram with altitude from geoportal
Autor: Radek Chládek
Datum: 21.09.2023
Verze: 1.0
Copyright (c) 2023 Radek Chládek

Tento program je svobodný software: můžete ho šířit a upravovat podle
Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.

Tento program je distribuován v naději, že bude užitečný,
ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
Svobodné licenci GNU.

Tuto licenci jste měli obdržet spolu s tímto programem.
Pokud ne, podívejte se na http://www.gnu.org/licenses/.
"""

import rosbag_utils as ru
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
import time

#config start
VERBOSE = True
CREATE_CSVs = True
DRAW = True

#replace following 3 lines with the path to the rosbag
bag_filename = 'transtech_2023-08-09-10-35-47-filter.bag'
parentDirectory = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
bag_path = os.path.join(parentDirectory, "data", bag_filename)
#config end


RET_START_TIME = True
print(VERBOSE*"loading bag")
bag, start_time = ru.load_bag(bag_path, VERBOSE, RET_START_TIME)


# for road grade estimation EKF algorithm is needed: force, speed, altitude, mass, 
 
##############################################################################################
#FDI ODOMETRY
print(VERBOSE*"reading FDI messages for road grade data")
data_speed = []
data_force = []
data_altitude = []
data_gps = []

print(VERBOSE*"loading fix gim")
data = []
for topic, msg, t in bag.read_messages(topics=['/fix_gim']):
    data.append([ru.get_time(msg) - start_time, msg.utc_time,
                msg.latitude, msg.longitude])
df_fix_gim = pd.DataFrame(data, columns=['msg_time', 'utc_time', 
                                         'Latitude','Longitude'])


for topic, msg, t in bag.read_messages(topics=['/hypex_fdi_data', '/hypex_tram_info']):
    if topic == '/hypex_fdi_data': 
        data_speed.append([ru.get_time(msg) - start_time, msg.odom_speed, msg.odom_acceleration])
    elif topic == '/hypex_tram_info':
        data_force.append([ru.get_time(msg) - start_time, msg.force_actual, msg.mass_actual])

#last_tick_timestamp clock is not synced with the tram clock. the value is in microseconds
df_speed = pd.DataFrame(data_speed, columns=['msg_time','speed','accel'])
df_force = pd.DataFrame(data_force, columns=['msg_time','force_actual', 'mass'])

df_speed.drop_duplicates(ignore_index = True, inplace=True)
df_force.drop_duplicates(ignore_index = True, inplace=True)
df_fix_gim.drop_duplicates(ignore_index = True, inplace=True)

# creation of dataframe for speed and force together since they are saved with same frequency and same start time
velocity_force_df = pd.merge(df_speed,df_force,on='msg_time',how='inner')

# now merge velocity_force with gps data (different frequency)
# Použijte funkci merge_asof k sloučení DataFramů podle blízkého času
merged_df = pd.merge_asof(velocity_force_df, df_fix_gim, left_on='msg_time', right_on='msg_time', direction='nearest')

merged_df.to_csv("road_grade_dataset_2023-08-09-10-35-47.csv",index=False)

    
