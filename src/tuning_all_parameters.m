clc,clear;

dat = readtable("plain_0000.csv", "ExpectedNumVariables", 6);

%Output Noise No filtering 0.6 mg rms
%default:0.00019247
accel_noise_rms = (0.6*10^(-3)*9.8)^2; 

%ADIS16465-1, 1 σ, no filtering, x-axis 0.05 °/sec rms
%ADIS16465-1, 1 σ, no filtering, y-axis and z-axis 0.07 °/sec rms
%default:9.1385e-5 
gyro_noise_rms = (deg2rad(0.06))^2; 

%In-Run Bias Stability ADIS16465-1, 1 σ 2 °/hr 
%default:3.0462e-13 
gyro_drift_noise = (deg2rad(2/3600))^2;

%Best fit straight line, ±2 g 0.25 % FS
%Best fit straight line, ±8 g, x-axis 0.5 % FS
%Best fit straight line, ±8 g, y-axis and z-axis 1.5 % FS
%default: 0.5
linear_accel_decay = 0.5;

%creation of ground_truth - we know it wasnt moving, so everything is 0
N = size(dat);
size_of_q = N(1);
ground_truth_d = zeros(size_of_q,3);
ground_truth_quaternions = eul2quat(ground_truth_d);
ground_truth_quaternions = quaternion(ground_truth_quaternions);

dat(:,1) = array2table(deg2rad(table2array(dat(:,1))));
dat(:,2) = array2table(deg2rad(table2array(dat(:,2))));
dat(:,3) = array2table(deg2rad(table2array(dat(:,3))));
dat = mergevars(dat, {'X_GYRO_16Bit_','Y_GYRO_16Bit_','Z_GYRO_16Bit_'}, 'NewVariableName', 'Gyroscope', 'MergeAsTable', false);
dat = mergevars(dat, {'X_ACCL_16Bit_','Y_ACCL_16Bit_','Z_ACCL_16Bit_'}, 'NewVariableName', 'Accelerometer', 'MergeAsTable', false);
dat=movevars(dat,'Gyroscope','After','Accelerometer');

ground_truth = array2table(ground_truth_quaternions);
ground_truth = renamevars(ground_truth,"ground_truth_quaternions","Orientation");

filter = imufilter("LinearAccelerationDecayFactor",0.5,'ReferenceFrame','ENU');

tic;
cfg = tunerconfig('imufilter','MaxIterations',50,'StepForward',100);
cfg.TunableParameters = setdiff(cfg.TunableParameters,'LinearAccelerationDecayFactor');
tune(imufilter, dat, ground_truth , cfg);
toc;

