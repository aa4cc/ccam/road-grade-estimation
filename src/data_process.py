'''

code which makes from raw files processed files into directory: processed 

Example usage: python3 data_process.py gps 2 1 1668412122.48449 108933 106 2000 experiment_skoda 13 4 600000 224000

@author: Radek Chládek

@contact: chladrad@fel.cvut.cz

@copyright: (c) 2022

'''

import pandas as pd
import sys
from datetime import datetime,timedelta,timezone
from csv import reader,writer
import os 

# TODO: better handeling more gps files 
#       error handling in input parameters
#       better interface  


def gps_txt2csv(file_name,number_of_files,time_shift):

    gps_data_frames = []

    for i in range(number_of_files):
        
        file_path = file_name + "_" + str(i) + ".txt"
        print("Now transforming file: " + file_path)
        
        dataframe1 = pd.read_csv(file_path, delim_whitespace=True)
        values = dataframe1.values 
        new_values = []

        for value in values:
            if ":" not in value[1]:
                x = int(previous_GPS_time[10])+1    
                corrected_GPS = previous_GPS_time[:10] + str(x) + ".000"
                corrected_time = previous_time[:6] + "00.000"
                corrected_date = previous_date
                date_str = str(corrected_date + " " + corrected_time)
                date = datetime.strptime(date_str,"%m/%d/%Y %H:%M:%S.%f")
                date += timedelta(hours=time_shift)
                date_synchronized_format = date.strftime("%Y-%m-%d %H:%M:%S.%f")
                new_type = [value[0],corrected_GPS,date_synchronized_format,value[4],value[5],value[6]]
                new_values.append(new_type)
            else:
                date_str = str(value[3] + " " + value[2])
                date = datetime.strptime(date_str,"%m/%d/%Y %H:%M:%S.%f")
                date += timedelta(hours=time_shift)
                date_synchronized_format = date.strftime("%Y-%m-%d %H:%M:%S.%f")
                new_type = [value[0],value[1],date_synchronized_format,value[4],value[5],value[6]]
                new_values.append(new_type)

            previous_GPS_time = value[1]
            previous_time = value[2]
            previous_date = value[3]

        final_dataframe = pd.DataFrame(new_values,columns=['Index','GPS time', 'TimeStamp', 'Lat','Lon','Speed'])
        gps_data_frames.append(final_dataframe)
        
        new_gps_file = "processed/GPS_and_IMU/"+file_name+ "_" + str(i) + ".csv"
        final_dataframe.to_csv(new_gps_file)

        print("Done transforming : " + file_path + " into " + new_gps_file)

    return gps_data_frames

def adding_timestamps2imu_data(timestamp,index_IMU,index_IPHONE,Fs,experiment_name,number_of_files,number_of_ints_in_name_of_file,samples_per_file,samples_on_last_file,gps_data_frames):
    
    #timestamp nalezeny z iphone dat na indexu 106, ktery odpovida indexu 108933 z IMU
    # timestamp = 1668412122.48449
    # index_IMU = 108933
    # index_IPHONE = 106

    dt = datetime.fromtimestamp(timestamp)
    print("Starting time : "+ str(dt) + " on index of imu file: " + str(index_IMU) + " index on iphone file : " + str(index_IPHONE))

    #frequency of writing of IMU sensor
    # Fs = 2000

    #basic name of experiment file in which u store data from IMU
    # experiment_name = "experiment_skoda_"

    # creating a list to store the
    # existing data of CSV file
    starting_index = index_IMU
    difference_of_seconds = starting_index*(1/Fs)
    time = dt-timedelta(seconds=difference_of_seconds)
    
    list_of_dataframes = []

    for i in range(number_of_files):
        file_name = experiment_name + "_" + str(i).zfill(number_of_ints_in_name_of_file) + ".csv" 
        time_row = []

        if i == number_of_files-1:
            ending_index = samples_on_last_file
        else:
            ending_index = samples_per_file
        print("Now adding timestamps to file : " + file_name)
        for j in range(0,ending_index):
            time += timedelta(seconds=1/Fs)
            time_row.append(time)
        new_file_name = "processed/IMU/"+experiment_name + str(i).zfill(number_of_ints_in_name_of_file) + "_timestamps" + ".csv"
        
        data_new = pd.read_csv(file_name,sep=";",decimal=",")
        data_new['TimeStamp'] = time_row
        
        data_new.to_csv(new_file_name,decimal=".",sep=",")
        list_of_dataframes.append(pd.read_csv(new_file_name))
        
        print("File: " + file_name + " has all timestamps")
    
    merged_df =[] 
    print("Now merging IMU files with GPS files into one: ")
    for j in range(number_of_files):
        merged_df.append(list_of_dataframes[j])

    merged_file_name = "processed/GPS_and_IMU/" + "GPS_and_IMU_and_notes" +"_merged.csv"
    done = pd.concat(merged_df, ignore_index=True)

    done["TimeStamp"] = pd.to_datetime(done["TimeStamp"])
    gps_data_frames[0]["TimeStamp"] = pd.to_datetime(gps_data_frames[0]["TimeStamp"])

    colnames=['TimeStamp', 'messageType', 'poznamka'] 
    dataframe1 = pd.read_csv("experiment_30_11_2022.txt",names=colnames, header=None)
    dataframe1['TimeStamp'] = pd.to_datetime(dataframe1["TimeStamp"])
    
    print(dataframe1)

    done_gps_imu = pd.merge_asof(gps_data_frames[0].sort_values('TimeStamp'),done.sort_values('TimeStamp'),on= 'TimeStamp')
    done_gps_imu = pd.merge_asof(done_gps_imu.sort_values('TimeStamp'),dataframe1.sort_values('TimeStamp'))
    del done_gps_imu['Unnamed: 0']

    #done.merge_asof(gps_data_frames,left_on='TimeStamp', right_on='TimeStamp', how='left')
    done_gps_imu.to_csv(merged_file_name)
    print(done_gps_imu)
    print("Files merged into : " + merged_file_name)

def main():

    if len(sys.argv) != 13:
        print("!!!!!!Missing some argument!!!!!!")
        print("------------------------------------Arguments must be in this exact form:--------------------------------------")
        print("<name_of_gps_file_without_.txt> <int=number_of_gps_files> <int=time_shift_in_hours> <timestamp> <index_IMU> <index_IPHONE> <Fs> <experiment_name> <number_of_files_IMU> <number_of_ints_in_name_of_file> <samples_per_file> <samples_on_last_file>")
        print("-----------------------------------------------------------------------------------------------------------------------------")
        print("Example usage: python3 data_process.py gps 2 1 1668412122.48449 108933 106 2000 experiment_skoda 13 4 600000 224000")
    else:
        # GPS files pieces of information
        file_name = sys.argv[1]
        number_of_files_gps = int(sys.argv[2])
        time_shift = int(sys.argv[3])

        # IMU files pieces of information
        timestamp = float(sys.argv[4])
        index_IMU = int(sys.argv[5])
        index_IPHONE = int(sys.argv[6])
        Fs = int(sys.argv[7])
        experiment_name = sys.argv[8]
        number_of_files_IMU = int(sys.argv[9])
        number_of_ints_in_name_of_file = int(sys.argv[10])
        samples_per_file = int(sys.argv[11])
        samples_on_last_file = int(sys.argv[12])

        if not os.path.exists("processed"):
        
            # if the demo_folder directory is not present 
            # then create it.
            os.makedirs("processed")

        if not os.path.exists("processed/IMU"):
        
            # if the demo_folder directory is not present 
            # then create it.
            os.makedirs("processed/IMU")
        
        if not os.path.exists("processed/GPS_and_IMU"):
        
            # if the demo_folder directory is not present 
            # then create it.
            os.makedirs("processed/GPS_and_IMU")

        
        gps_data_frames = gps_txt2csv(file_name,number_of_files_gps,time_shift)
        adding_timestamps2imu_data(timestamp,index_IMU,index_IPHONE,Fs,experiment_name,number_of_files_IMU,number_of_ints_in_name_of_file,samples_per_file,samples_on_last_file,gps_data_frames)


if __name__ == "__main__":
    main()