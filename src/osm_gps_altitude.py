"""
Název: osm_gps_altitude
Popis:
Autor: Radek Chládek
Datum: 21.09.2023
Verze: 1.0
Copyright (c) 2023 Radek Chládek

Tento program je svobodný software: můžete ho šířit a upravovat podle
Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.

Tento program je distribuován v naději, že bude užitečný,
ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
Svobodné licenci GNU.

Tuto licenci jste měli obdržet spolu s tímto programem.
Pokud ne, podívejte se na http://www.gnu.org/licenses/.
"""

import overpy
import folium
import pandas as pd
import rasterio
import numpy as np

print("Connecting to OSM")

# connection to OSM API
api = overpy.Overpass()

# id of your desired tram relation
relation_id = 2441935

# query for relation ID 2441935
query = f"""
rel({relation_id});
out body;
>;
out skel qt; """

# query send to OSM API
result = api.query(query)

gps_coordinates = []

print("Reading GPS coordinates")

# GPS coords for all points in relation
for node in result.nodes:
    gps_coordinates.append((node.lat,node.lon))


print("I found " + str(len(gps_coordinates)) + " GPS coordinates on this relation")

# creation of dataframe
df = pd.DataFrame(gps_coordinates, columns=["Latitude","Longitude"])

# name of CSV file
csv_file = "GPS_relation_" + str(relation_id) + ".csv"

gps_coord = [[row[1], row[0]] for row in gps_coordinates]

dsm_data =  rasterio.open('dsm_1m_wsg84.tif')
dtm_data =  rasterio.open('dtm_1m_wsg84.tif')

alt_dsm = np.fromiter(dsm_data.sample(gps_coord), dtype=np.float64)
alt_dtm = np.fromiter(dtm_data.sample(gps_coord), dtype=np.float64)


df["Altitude_dsm"] = alt_dsm
df["Altitude_dtm"] = alt_dtm

# saving CSV
df.to_csv(csv_file, index=False)

print("GPS coordinates saved to: " + csv_file)

# how much initially zoom your map
zoom = 13.5
# creation of map
m = folium.Map(location=[gps_coordinates[len(gps_coordinates)//2][0], gps_coordinates[len(gps_coordinates)//2][1]], zoom_start=zoom)

# adding coords to map
for coord in gps_coordinates:
    folium.Marker(location=[coord[0], coord[1]]).add_to(m)

map_name = "map_of_relation_" + str(relation_id) +".html"
# save_map
m.save(map_name)  

print("Map visualization of GPS coordinates is saved to : " + map_name)