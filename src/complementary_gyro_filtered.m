dat = readtable("emu16_08_12_52.csv", "ExpectedNumVariables", 6);

x_gyro = table2array(dat(:, 1));
y_gyro = table2array(dat(:, 2));
z_gyro = table2array(dat(:, 3));
x_accel = table2array(dat(:,4));
y_accel = table2array(dat(:,5));
z_accel = table2array(dat(:,6));

x_gyro_fil = deg2rad(myWindow(x_gyro, 2000));
y_gyro_fil = deg2rad(myWindow(y_gyro, 2000));
z_gyro_fil = deg2rad(myWindow(z_gyro, 2000));

gyro = [x_gyro_fil y_gyro_fil z_gyro_fil];
accel = [x_accel y_accel z_accel];
Fs = 2000;
fuse = complementaryFilter('SampleRate', Fs,'HasMagnetometer',false,'AccelerometerGain',0.2);
q = fuse(accel, gyro);
plot(eulerd(q, 'ZYX', 'frame'));
title('Orientation Estimate');
legend('Z-rotation', 'Y-rotation', 'X-rotation');
ylabel('Degrees');

function y = myWindow(x, windowSize)
b = (1/windowSize)*ones(1,windowSize);
a = 1;
y = filter(b, a, x);
end

function y = array_int(x, Ts)
u(1:length(x)) = 0;
u(1) = x(1);
for i = 2:length(x)
    u(i) = u(i-1) + x(i)*Ts;
end
y = u;
end
