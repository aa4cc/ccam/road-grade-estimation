// Definition size of housing
// **************************

       // Those settings have to be identical to the cover !!!
width_x      = 80;          // Width of the housing (outer dimension)
wall         = 1.8;   
debth_y      = 45 + 2*wall;           // Debth of the housing (outer dimension)       // Wall thickness of the box
cornerradius = 4.8;                             //   This value also defines the posts for stability and
block_size = 7;                             //   for the press-in nuts!
block_shift = 0.5;

       // Those settings are more or less independent from the cover
height_z     = 35;           // Heigth of the lower part. Total height is this value plus
                             //   the height of the cover
plate        = 1.8;          // Thickness of the bottom plain

       //Definition size of press-in nuts
nutdiameter  = 1.6;          // Hole diameter for thermal press-in nut
nutlength    = 5.8;          // Depth of the nut hole


//Definition of circle angular resolution
resol        = 36;


// sinking of nut holes
sink = 4;

// parametres of eval kit
connectors_width = 32;
connectors_height = 7;
eval_depth = 38;

// imu nut housing parametres
housing_a = 5;
nut_d = 4.5;
housing_depth = 2.5;
taper_height = 0.2;
screw_d = 2;
imu_width = 42;
imu_depth = 39;
union(){
    difference () {

    // Construction of housing
       union () {
       // Definition of main body
          difference () {
             union () {
                cube ( [width_x, debth_y - (2* cornerradius), height_z], center = true );
                cube ( [width_x - (2* cornerradius), debth_y, height_z], center = true );
             };
             translate ( [0,0, plate] ){
                cube ( [width_x - (2* wall), debth_y- (2* wall), height_z], center = true ); 
             };
          };
        
          // corners
          intersection(){
              difference(){
                  cube ( [width_x, debth_y, height_z], center = true );
                  translate ( [0,0, plate / 2] ){
                    cube ( [width_x - (2* wall), debth_y- (2* wall), height_z - plate], center = true ); 
                };
              };
              union(){
                  // 1st quadrant
                  translate ( [(width_x / 2) - cornerradius, (debth_y / 2) - cornerradius, 0] ) {
                    cylinder (h=height_z, r=cornerradius, center = true, $fn = resol);
                  };
                  // 2nd quadrant
                  translate ( [-(width_x / 2) + cornerradius, (debth_y / 2) - cornerradius, 0] ) {
                    cylinder (h=height_z, r=cornerradius, center = true, $fn = resol);
                  };
                  // 3rd quadrant
                  translate ( [-(width_x / 2) + cornerradius, -(debth_y / 2) + cornerradius, 0] ) {
                    cylinder (h=height_z, r=cornerradius, center = true, $fn = resol);
                  };
                  // 4th quadrant
                  translate ( [(width_x / 2) - cornerradius, -(debth_y / 2) +  cornerradius, 0] ) {
                    cylinder (h=height_z, r=cornerradius, center = true, $fn = resol);
                  };
              };
          };
          
          
          // Construction of four corner blocks including nut holes
          // 1st quadrant
          translate ( [(width_x - block_size)/2 - wall, eval_depth / 2, -sink/2 ] ) {
             left_corner_block();
          };

          // 2nd quadrant
          translate ( [-(width_x - block_size)/2 + wall, eval_depth / 2, -sink/2] ) {
             left_corner_block();
          };

          // 4th quadrant
          translate ( [(width_x - block_size)/2 - wall, -eval_depth / 2, -sink/2] ) {
              rotate([0, 0, 180]){
                left_corner_block();
              };
          };

          // 3rd quadrant
          translate ( [-(width_x - block_size)/2 + wall, -eval_depth / 2, -sink/2] ) {
             rotate([0, 0, 180]){
                left_corner_block();
              };
          };
          
          
          
       };

    // Space for the construction of holes, breakouts, ... 
       translate([(width_x-wall) / 2, 0, height_z / 2 - (sink + connectors_height) / 2]){
           cube([width_x, connectors_width, sink + connectors_height], center=true);
       };
       
       // space for imu nut housings
       translate([width_x/2-(2*cornerradius+housing_a/2), imu_depth/2, -height_z/2]){
            cube(housing_a, center=true);
       };
       translate([width_x/2-(2*cornerradius+housing_a/2), -imu_depth/2, -height_z/2]){
            cube(housing_a, center=true);
       };
       translate([width_x/2-(2*cornerradius+housing_a/2)-imu_width, imu_depth/2, -height_z/2]){
            cube(housing_a, center=true);
       };
       translate([width_x/2-(2*cornerradius+housing_a/2)-imu_width, -imu_depth/2, -height_z/2]){
            cube(housing_a, center=true);
       };
    };
    translate([width_x/2-(2*cornerradius+housing_a/2), imu_depth/2, -height_z/2]){
            nut_housing();
       };
       translate([width_x/2-(2*cornerradius+housing_a/2), -imu_depth/2, -height_z/2]){
            nut_housing();
       };
       translate([width_x/2-(2*cornerradius+housing_a/2)-imu_width, imu_depth/2, -height_z/2]){
            nut_housing();
       };
       translate([width_x/2-(2*cornerradius+housing_a/2)-imu_width, -imu_depth/2, -height_z/2]){
            nut_housing();
       };
};


module nut_housing(){
    difference(){
        union(){
            translate([0, 0, housing_a/4]){
                cube([housing_a, housing_a, housing_a/2], center=true);
            };
            rotate([0, 0, 45]){
                translate([0, 0, housing_a/2]){
                    cylinder(h=housing_a/2, d1=sqrt(2)*housing_a, d2=screw_d+wall, $fn = 4, center=false);
                };
            };
        };
        union(){
            cylinder (h=housing_depth, d=nut_d, center = false, $fn = 6);
            translate([0, 0, housing_depth]){
                cylinder (h=taper_height, d1=nut_d, d2=screw_d, center = false, $fn = 6);
            };
        };
        translate([0, 0, housing_a/2]){
        cylinder (h=housing_a+1, d=screw_d, center = true, $fn = resol);
        };
    };
};

module left_corner_block(){
    difference(){
        translate([0, block_shift/2, 0]){
            cube([block_size, block_size-block_shift, height_z - sink], center=true);
        };

        translate ( [ 0,0,((height_z - sink) / 2)-(nutlength / 2) ] ) {
            cylinder (h = nutlength, r = nutdiameter / 2, center = true, $fn = resol);
        };
    };
};