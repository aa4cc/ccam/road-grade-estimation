'''
code for adding timestamps into already esixting files from IMU sensor. It is required to have at least one timestamp from external source
@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2022
'''

# Python 3.2 or higher required
from datetime import datetime, timezone, timedelta
from csv import reader,writer

def add_column_in_csv(input_file, output_file,new_column):
    default_text = 'Some Text'
    # Open the input_file in read mode and output_file in write mode
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = writer(write_obj)
        # Read each row of the input csv file as list
        idx = 0
        for row in csv_reader:
            # Append the default text in the row / list
            row.append(new_column[idx])
            # Add the updated row / list to the output file
            csv_writer.writerow(row)
            idx += 1


def main():
    #timestamp nalezeny z iphone dat na indexu 106, ktery odpovida indexu 108933 z IMU
    timestamp = 1668412122.48449
    index_IMU = 108933
    index_IPHONE = 106

    dt = datetime.fromtimestamp(timestamp)
    print("Starting time : "+ str(dt) + " on index of imu file: " + str(index_IMU) + " index on iphone file : " + str(index_IPHONE))

    #frequency of writing of IMU sensor
    Fs = 2000

    #basic name of experiment file in which u store data from IMU
    experiment_name = "experiment_skoda_"

    # creating a list to store the
    # existing data of CSV file
    starting_index = index_IMU
    difference_of_seconds = starting_index*(1/Fs)
    time = dt-timedelta(seconds=difference_of_seconds)
    time_row = ["TimeStamp"]

    for i in range(13):
        file_name = experiment_name + str(i).zfill(4) + ".csv"
        if i == 12:
            ending_index = 224001
        else:
            ending_index = 600001
        print("Now adding timestamps to file : " + file_name)
        for j in range(0,ending_index):
            time += timedelta(seconds=1/Fs)
            time_row.append(time)
        new_file_name = experiment_name + str(i).zfill(4) + "_timestamps" + ".csv"
        add_column_in_csv(file_name,new_file_name,time_row)
        time_row = ["TimeStamp"]
        print("File: " + file_name + " has all timestamps")

if __name__ == "__main__":
    main()