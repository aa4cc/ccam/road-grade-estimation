"""
Název: merge_tram_OSM_kNn
Popis:
Autor: Radek Chládek
Datum: 21.09.2023
Verze: 1.0
Copyright (c) 2023 Radek Chládek

Tento program je svobodný software: můžete ho šířit a upravovat podle
Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.

Tento program je distribuován v naději, že bude užitečný,
ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
Svobodné licenci GNU.

Tuto licenci jste měli obdržet spolu s tímto programem.
Pokud ne, podívejte se na http://www.gnu.org/licenses/.
"""

import pandas as pd
from sklearn.neighbors import NearestNeighbors
import time

start_time = time.time()  # start

# read altitude with gps from OSM and GEOPORTAL
df1 = pd.read_csv('altitude.csv')

# read road_grade_dataset from tram
df2 = pd.read_csv('road_grade_dataset.csv')

# number of nearest Nearest Neighbors we want to find
n_neighbors = 1

nn = NearestNeighbors(n_neighbors=n_neighbors, algorithm='auto', leaf_size=30, metric='sqeuclidean', p=2, n_jobs=-1)

# train model from GPS tram
nn.fit(df2[['Latitude', 'Longitude']],df1[['Latitude', 'Longitude']])

# Find Nearest Neighbor
distances, indexy = nn.kneighbors(df1[['Latitude', 'Longitude']])

# add nearest coord for manual check
spojeny_df = df1.copy()  
spojeny_df['closest_latitude'] = df2.loc[indexy[:, 0], 'Latitude'].values
spojeny_df['closest_longitude'] = df2.loc[indexy[:, 0], 'Longitude'].values

# adding force speed and mass
added_columns = ['speed', 'force_actual', 'mass','accel']
for sloupec in added_columns:
    spojeny_df[sloupec] = df2.loc[indexy[:, 0], sloupec].values

# save merged csv
spojeny_df.to_csv("merged_kNn.csv",index=False)

end_time = time.time()    # end
elapsed_time = end_time - start_time
print(f"Čas vykonání kódu: {elapsed_time} sekund")
