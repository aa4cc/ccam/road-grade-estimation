% all distances in mm

% data loading
dat = readtable("ev3_16_08_13_30_2", "ExpectedNumVariables", 5);
time_dat = dat.time;
angleL = dat.angle_left;
angleR = dat.angle_right;
color = dat.control_lines_data_color;
dat2 = readtable("emu16_08_13_30", "ExpectedNumVariables", 6);
emu_n_samples = length(dat2.X_GYRO_16Bit_);

% time normalization (start on 0, time is seconds)
time_start = time_dat(1);
time_dat = time_dat - time_start;
time_dat = time_dat/1000;

%%% distance constants
% measured distances between green lines (signalization) and angle change
intersection_delay(1) = 78;
intersection_delay(2) = 72;
intersection_delay(3) = 80;
intersection_delay(4) = 92;
intersection_delay(5) = 90;
intersection_delay(6) = 99;
intersection_delay(7) = 57;
intersection_delay(8) = 63;
intersection_delay(9) = 53;
intersection_delay(10) = 53;

% vehicle length
intersection_length = 165;

% vehicle wheel parametres
diameter = 56;
perimeter = pi * diameter;
%%%

preride = 0; % time before ride
sections(1:11) = 0; % section (parts of track with constant angle) lengths
intersections(1:10) = 0; % intersection (parts of track with varient angle) lengths

sec_counter = 1;
intersec_counter = 1;
green = false;
green_dist = 0;

for i=1:length(time_dat)
    if(preride == 0)
        if(color(i) == 1)
            preride = time_dat(i);
        end
    elseif(intersec_counter == 11)
        break
    elseif(sec_counter == intersec_counter)
        if(green)
            green_dist = green_dist + gained_dist(abs(angleL(i) - angleL(i - 1)), abs(angleR(i) - angleR(i - 1)), perimeter);
            if(green_dist >= intersection_delay(sec_counter))
                % angle is changing continously
                intersec_dist = intersection_delay(sec_counter) - green_dist;
                green_dist = 0;
                sections(sec_counter) = time_dat(i);
                sec_counter = sec_counter + 1;
                green = false;
            end
        elseif(color(i) == 3)
            green = true;
        end
    else
        intersec_dist = intersec_dist + gained_dist(abs(angleL(i) - angleL(i - 1)), abs(angleR(i) - angleR(i - 1)), perimeter);
        if(intersec_dist >= intersection_length)
            intersections(intersec_counter) = time_dat(i);
            intersec_counter = intersec_counter + 1;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% section measures definition and angles approximation
lengths(1) = 260;
elevations(1) = 0;
distances(1) = 260;

lengths(2) = 835;
elevations(2) = 225;
distances(2) = 865;

lengths(3) = 867;
elevations(3) = 0;
distances(3) = 867;

lengths(4) = 562;
elevations(4) = -150;
distances(4) = 582;

lengths(5) = 282;
elevations(5) = 0;
distances(5) = 282;

lengths(6) = 279;
elevations(6) = -75;
distances(6) = 289;

lengths(7) = 287;
elevations(7) = 0;
distances(7) = 287;

lengths(8) = 514;
elevations(8) = 112;
distances(8) = 526;

lengths(9) = 638;
elevations(9) = 0;
distances(9) = 638;

lengths(10) = 519;
elevations(10) = -112;
distances(10) = 531;

lengths(11) = 170;
elevations(11) = 0;
distances(11) = 170;

intersection_length = 168; % distance between vehicle's axes

n_sections = length(lengths);
angles(1:n_sections) = 0;
for i = 1:n_sections
    ang(1) = acos(lengths(i)/distances(i));
    ang(2) = asin(abs(elevations(i))/distances(i));
    ang(3) = atan(abs(elevations(i))/lengths(i));
    if(elevations(i)<0)
        sign = -1;
    else
        sign = 1;
    end
    angles(i) = sign * rad2deg(mean(ang));
end

decim = 1;
Fs = 2000;
time = ((0:decim:emu_n_samples-1)/Fs);



actual = 1;
clear true_gyro;
true_gyro(1:length(time)) = 0;
for i = 1:length(time)
    if(and(actual < n_sections,time(i) > sections(actual)))
        if(time(i) < intersections(actual))
            over_section = time(i) - sections(actual);
            intersectioning_parameter = over_section / (intersections(actual) - sections(actual));
            true_gyro(i) = (1-intersectioning_parameter) * angles(actual) + intersectioning_parameter * angles(actual + 1);
            continue;
        else
            actual = actual + 1;
        end
    end
    true_gyro(i) = angles(actual);
end
figure(1);
plot(time, true_gyro)


function y = gained_dist(rotationL, rotationR, perimeter)
    y = (rotationL + rotationR) * perimeter / 720;
end