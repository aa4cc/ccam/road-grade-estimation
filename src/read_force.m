%{
data processing of Skoda Transportaion data from electric train 16Ev
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
usage:
comment out block with profiles, set time for 190 seconds, set v_ref to 25 constant 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clc,clear;
konstanty_model;

%% loading data
dat_force_m = readtable("ato-g1_aru-rec_20230316-155745_ARU/data_in_csv/ad12627_reg_forcesp_1.aru.csv", "ExpectedNumVariables", 4);
dat_force_slope = readtable("ato-g1_aru-rec_20230316-155745_ARU/data_in_csv/ad12627_reg_slopefor_1.aru.csv", "ExpectedNumVariables", 4);
dat_accel = readtable("ato-g1_aru-rec_20230316-155745_ARU/data_in_csv/ad12627_odo_acceler_1.aru.csv", "ExpectedNumVariables", 4);
dat_speed = readtable("ato-g1_aru-rec_20230316-155745_ARU/data_in_csv/ad12627_odo_speed_1.aru.csv", "ExpectedNumVariables", 4);
dat_speedrq = readtable("ato-g1_aru-rec_20230316-155745_ARU/data_in_csv/ad12627_reg_speedrq_1.aru.csv", "ExpectedNumVariables", 4);

end_index = 2000;
x_classic = table2array(dat_force_m(1:end_index,2));
x_slope = table2array(dat_force_slope(:,2));

F_m = table2array(dat_force_m(1:end_index,3));
F_s = table2array(dat_force_slope(:,3));
accel = table2array(dat_accel(1:end_index,3));
speed = table2array(dat_speed(1:end_index,3));
speed_rq = table2array(dat_speedrq(1:end_index,3));

%% slope alpha 
alpha = asind(F_s./(m*g));


%% comparasion of simulated and real data
start_offset = 75394.2;
x_s = out.x_s.signals.values;
x_classic = x_classic - start_offset;

F_m_s = out.F_m_s.signals.values;
v_s = out.v_s.signals.values;
%alpha_s = asind(out.sin_a.signals.values);
a_s = out.a_s.signals.values;
t_s = out.F_m_s.time;

number_of_elements = length(x_s);

F_m_q = interp1(x_s,F_m_s,x_classic);
v_q = interp1(x_s,v_s,x_classic);
%alpha_q = interp1(x_s,alpha_s,x_classic);
a_q = interp1(x_s,a_s,x_classic);

font_size = 14;

end_index = 1258;
figure("Name","Síla motoru");
plot(x_classic(1:end_index),F_m(1:end_index),"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic(1:end_index),F_m_q(1:end_index),"LineWidth",2,"LineStyle","--","Color","red");
lgd = legend('Naměřená data','Simulovaná data');
fontsize(lgd,font_size,'points');
xlabel('s [m]','FontSize',font_size);
ylabel('F_{motoru} [N]','FontSize',font_size);
hold off;

figure("Name","Rychlost");
plot(x_classic(1:end_index),speed(1:end_index),"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic(1:end_index),v_q(1:end_index),"LineWidth",2,"Color","red","LineStyle","--");
lgd = legend('Naměřená data','Simulovaná data');
fontsize(lgd,font_size,'points');
xlabel('s [m]','FontSize',font_size);
ylabel('v [m/s]','FontSize',font_size);
hold off;

figure("Name","Zrychlení");
plot(x_classic(1:end_index),accel(1:end_index),"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_classic(1:end_index),a_q(1:end_index),"LineWidth",2,"Color","red","LineStyle","--");
lgd = legend('Naměřená data','Simulovaná data');
fontsize(lgd,font_size,'points');
xlabel('s [m]','FontSize',font_size);
ylabel('a [m/s^2]','FontSize',font_size);
hold off;

% figure("Name","úhel sklonu");
% plot(x_classic,alpha_q,"LineWidth",2,"LineStyle","--","Color","red");
% hold on;
% grid on;
% plot(x_slope,alpha,"LineWidth",2,"Color","blue");
% legend('Simulovaná data','Naměřená data');
% xlabel('index [~]');
% ylabel('s [m]');
% hold off;


