#!/usr/bin/env pybricks-micropython
'''
ev3 line following robot with steering motor for road grade estimation
@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2022
'''

from pybricks.ev3devices import Motor, ColorSensor
from pybricks.parameters import Port, Button, Stop, Color
from pybricks.tools import DataLog, StopWatch, wait
from pybricks.robotics import DriveBase
from pybricks.hubs import EV3Brick

# Initialize the EV3    
ev3 = EV3Brick()

# Tuning parameters
gear = 4
SPEED_MAX = -50 * gear
SPEED_TURN = -25 * gear
SPEED_OFFLINE = -40 * gear

# Initialize the hub, motors, and sensor
steer_motor = Motor(Port.A)
drive_motor_left = Motor(Port.B)
drive_motor_right = Motor(Port.C)
light_sensor = ColorSensor(Port.S3)
watch = StopWatch()
control_lines_sensor = ColorSensor(Port.S4)
data = DataLog('time', 'angle_left', 'angle_right', 'angle_steer', 'control_lines_data_color')

# Use the color saturation value to track line
def get_light(control=False):
    '''
    gets the light information from the sensors
    parameters: control - if True gets control lines sensor, line sensor otherwise
    returns: control: number of a color, light_reflection: percantage of the light reflection 
    '''
    if control:
        #instead of the color name it will write down our indicator number
        indicator = 999
        color = control_lines_sensor.color()
        if color == Color.WHITE:
            indicator = 0
        elif color == Color.BLACK:
            indicator = 1
        elif color == Color.RED:
            indicator = 2
        elif color == Color.GREEN:
            indicator = 3
        elif color == Color.BLUE:
            indicator = 4
        return indicator
    return light_sensor.reflection()

def calibrate():
    '''
    parameters: None
    returns: None
    automatically calibrates(gets the left and right limit) the steering motor
    '''
    global a_steer_limit

    ''' 
    optimal settings:
        Right limit:  190
        Left limit:  -19
        Steer limit:  104
    '''

    # Find the Right and Left hard limits
    a_right_limit = steer_motor.run_until_stalled(
        400, then=Stop.BRAKE, duty_limit=100
    )
    print("Right limit: ", a_right_limit)
    a_left_limit = steer_motor.run_until_stalled(
        -400, then=Stop.BRAKE, duty_limit=100
    )


    print("Left limit: ", a_left_limit)

    # Calculate the steering limit as average of two extremes then
    # reset angle to the negative limit since steering motor is now
    # at negative extreme
    a_steer_limit = (a_right_limit - a_left_limit) // 2
    steer_motor.reset_angle(-a_steer_limit)
    print("Steer limit: ", a_steer_limit)

    # Center the steering
    steer_motor.run_target(1000, 0, then=Stop.BRAKE, wait=True)

def calibrate_light():
    '''
    parameters: None
    returns: None
    gets the reflection of black and white using button
    '''
    global l_min, l_max, l_mid

    print("Nahraj bilou a mackni jakekoliv tlacitko:")
    while(not(ev3.buttons.pressed())):
        wait(50)
    l_max = get_light()
    print(l_max)
    
    wait(500)

    print("Nahraj cernou a mackni jakekoliv tlacitko:")
    while(not(ev3.buttons.pressed())):
        wait(50)
    l_min = get_light()
    print(l_min)

    l_mid = (l_min + l_max)/2

def logg_data():
    '''
    parameters: None
    returns: None
    loggs data to file
    '''
    #DataLog('time', 'angle_left', 'angle_right', 'angle_steer','control_lines_data_color')
    global data

    time = watch.time()
    angle_left = drive_motor_left.angle()
    angle_right = drive_motor_right.angle()
    angle_steer = steer_motor.angle()
    control_lines_data_color = get_light(True)
    #control_lines_data_ambient = control_lines_sensor.ambient()
    data.log(time,angle_left,angle_right,angle_steer,control_lines_data_color)

robot = DriveBase(drive_motor_left, drive_motor_right, wheel_diameter=56, axle_track=169) 

global integral,derivative,last_error
integral = 0
derivative = 0
last_error = 0

def follow_line_PID():
    '''
    parameters: None
    returns: None
    following line using "PID" regulation, currently only usin PD.
    k_p = 1.8
    k_i = 0.0001
    k_d = 15
    '''
    #steering with steering motor
    global l_mid, l_max, l_min,integral,derivative,last_error
    
    discount_factor = 1
    k_p = 1.8
    k_i = 0.0001
    k_d = 15

    error = get_light()-l_mid
    integral = int(integral*discount_factor) + error
    if(error*last_error <= 0):
        # anti-windup, passing the mid-intesity value from either side will annul integrator
        integral = 0
    derivative = error - last_error

    turn_rate = 0.8*(k_p * (error) + k_i * (integral) + k_d * (derivative))

    # Clamp the target angle to within +- a_steer_limit
    turn_rate = min(turn_rate, a_steer_limit)
    turn_rate = max(turn_rate, -a_steer_limit)

    big_turn_const = a_steer_limit//2

    steer_motor.run_target(1000, turn_rate, then=Stop.BRAKE, wait=False)

    if turn_rate > big_turn_const or turn_rate < -big_turn_const:
        robot.drive(SPEED_TURN, 0)
    else:
        robot.drive(SPEED_MAX, 0)
    last_error = error

last_checked_time = 0
drive_speed = SPEED_MAX
time_period = 2000
stop = False

def follow_line_PID_with_stops():
    '''
    parameters: None
    returns: None
    following line using PID with stops before the change of road grade
    '''
    #steering with steering motor
    global l_mid, l_max, l_min,integral,derivative,last_error,time,last_checked_time,time_period,stop,drive_speed
    
    k_p = 1.8
    k_i = 0.0001
    k_d = 15
    
    error = get_light()-l_mid
    integral = integral + error 
    if(error*last_error <= 0):
        # anti-windup, passing the mid-intesity value from either side will annul integrator
        integral = 0

    derivative = error - last_error

    turn_rate = 0.8*(k_p * (error) + k_i * (integral) + k_d * (derivative))

    # Clamp the target angle to within +- a_steer_limit
    turn_rate = min(turn_rate, a_steer_limit)
    turn_rate = max(turn_rate, -a_steer_limit)

    big_turn_const = a_steer_limit//2

    steer_motor.run_target(1000, turn_rate, then=Stop.BRAKE, wait=False)

    if get_light(True) == 3 and stop == False:
        stop = True
        drive_speed = 0
        last_checked_time = watch.time()
    
    if  watch.time() - last_checked_time >= time_period and stop == True:
            drive_speed = SPEED_MAX
            stop = False
    
    if turn_rate > big_turn_const or turn_rate < -big_turn_const:
        robot.drive(drive_speed/2, 0)
    else:
        robot.drive(drive_speed, 0)
    
    last_error = error

def follow_line_bangbang():
    '''
    parameters: None
    returns: None
    following line using bang bang regulation. NOT TESTED
    '''

    #is not tested
    global l_mid, l_max, l_min 
    reflection = get_light()

    threshold_1 = int((l_max - l_min) / 3) + l_min
    threshold_2 = l_max - int((l_max - l_min)/3)
    
    if reflection <= threshold_1:
        #left
        steer_motor.run_target(1000, a_steer_limit, then=Stop.BRAKE, wait=False)
        robot.drive(SPEED_TURN, 0)
    elif reflection >= threshold_2:
        #right
        steer_motor.run_target(1000, -a_steer_limit, then=Stop.BRAKE, wait=False)
        robot.drive(SPEED_TURN, 0)
    else:
        #center
        steer_motor.run_target(1000, 0, then=Stop.BRAKE, wait=True)
        robot.drive(SPEED_MAX, 0)


#firstly there is an automatic calibration of the steering motor
#secondly there is manual calibration of the light
calibrate()

calibrate_light()

# Set max speed, acceleration, and max power for drive motor
drive_motor_left.stop()  # must be stopped to set limits
drive_motor_left.control.limits(1000, 2000, 100)
drive_motor_right.stop()  # must be stopped to set limits
drive_motor_right.control.limits(1000, 2000, 100)

#for sync of the accelerometr and our line sensor
while(get_light(True) != 4):
    wait(10)
  
ev3.speaker.beep(1,500)

while True:
    logg_data()
    follow_line_PID_with_stops()
