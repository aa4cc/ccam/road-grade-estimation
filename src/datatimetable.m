% preparing data (.csv) from imu to any kind of operation
% turning it into array, table, timetable
% Fs = sample frequency
 
% Radek Chládek
% 17.10.2022
dat = readtable("data/experiment_srpen_2022/emu16_08_12_52.csv", "ExpectedNumVariables", 6);

x_gyro = table2array(dat(:,1));
y_gyro = table2array(dat(:,2));
z_gyro = table2array(dat(:,3));
x_accel = table2array(dat(:,4));
y_accel = table2array(dat(:,5));
z_accel = table2array(dat(:,6));

x_gyro_rad = deg2rad(x_gyro);
y_gyro_rad = deg2rad(y_gyro);
z_gyro_rad = deg2rad(z_gyro);

dat(:,1) = array2table(x_gyro_rad);
dat(:,2) = array2table(y_gyro_rad);
dat(:,3) = array2table(z_gyro_rad);

gyro = [x_gyro_rad y_gyro_rad z_gyro_rad];
accel = [x_accel y_accel z_accel];
Fs = 2000;

dat_timetable = table2timetable(dat,'SampleRate',Fs);


