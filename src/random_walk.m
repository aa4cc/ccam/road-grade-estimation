%% random walk
rng(10);

konstanty_model;

x_s = out.x_s.signals.values;
v_s = out.v_w_noise.signals.values;
v_s_noise = out.v_s.signals.values;
random_walk_1 = out.random_walk.signals.values;

font_size = 14;

figure("Name","Náhodná procházka");
plot(x_s,random_walk_1,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('Náhodná procházka [~]','FontSize',font_size);
hold off;

figure("Name","RW rychlost");
plot(x_s,v_s,"LineWidth",2,"Color","blue");
hold on;
plot(x_s,v_s_noise,"LineWidth",2,"Color","red","LineStyle","--");
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
legend('Rychlost bez náhodné procházky','Rychlost s náhodnou procházkou','FontSize',font_size);
grid on;
hold off;
