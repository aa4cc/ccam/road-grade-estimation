%{
Implementation of Extended Kalman Filter
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

rng(10);

konstanty_model;
font_size = 14;

%% constants for simulink model of vehicle
g = 9.81;
m = 150000;
c_w = 1.05;
A_a = 14.4840;
c_r = 0.0005;
% nadmorska vyska = 273m, T = 10C 
rho_a = 1.201;

%% reading of simulated data
x_s = out.x_s.signals.values;
t_s = out.z_s.time;
alpha_s = out.alpha_s.signals.values;
v_s = out.v_s.signals.values;
z_s = out.z_s.signals.values;
F_m_s = out.F_m_s.signals.values;
n = length(x_s);

%% interpolation of dt to ds (ds = 2.5) 
ds = 2.5;
final_length = x_s(n);  

%my new "time"
s_q = 0:ds:final_length;

alpha_q = interp1(x_s,alpha_s,s_q);
v_q = interp1(x_s,v_s,s_q);
z_q = interp1(x_s,z_s,s_q);
F_m_q = interp1(x_s,F_m_s,s_q);

% saving all the measurements into one array 
measurements = [v_q; z_q];


%% random walk to velocity
noise = randn(1,length(v_q));
random_walk = cumsum(noise)/20;
measurements(1,:) = measurements(1,:) + random_walk;

figure("Name","Noise_velocity");
plot(s_q,v_q,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
plot(s_q,measurements(1,:),"LineWidth",2,"Color","red","LineStyle","--");
legend('Nezašuměná rychlost','Zašuměná rychlost','FontSize',font_size);
hold off;

%% white noise to altitude 
noise = randn(1,length(z_q));
measurements(2,:) = measurements(2,:) + noise;

figure("Name","Noise_altitude");
plot(s_q,z_q,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('altitude [m]','FontSize',font_size);
plot(s_q,measurements(2,:),"LineWidth",2,"Color","red","LineStyle","--");
legend('Nezašuměná nadmořská výška','Zašuměná nadmořská výška','FontSize',font_size);
hold off;

%% EKF 

% initialize EKF
% pro h
trueInitialState = [0.00001;121;0.00001];
% pro a 
%trueInitialState = [0.00001;329.3;0.00001]; % [v,z,alpha]
initialCovariance = diag([1,0.001,0.001]);
processNoise = diag([1; 1; 1]); % Process noise matrix
measureNoise = diag([1500000;900000]); % Measurement noise matrix. Units are m^2 and rad^2.

filter = trackingEKF(State=trueInitialState,StateCovariance=initialCovariance, ...
    StateTransitionFcn=@stateModel,ProcessNoise=processNoise, ...
    MeasurementFcn=@measureModel,MeasurementNoise=measureNoise);
estimateStates(:,1) = filter.State;

K = c_w*A_a*rho_a;

% Running of EKF
for i=2:length(s_q)
    predict(filter,ds,F_m_q(:,i),m,K,c_r);
    estimateStates(:,i) = correct(filter,measurements(:,i));
end


figure("Name","Alfa");
plot(s_q,rad2deg(estimateStates(3,:)),"LineWidth",2,"LineStyle","--","Color","red");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
plot(s_q,rad2deg(alpha_q),"LineWidth",2,"Color","blue");
legend('EKF','Model','FontSize',font_size);
hold off;

figure("Name","Rychlost");
plot(s_q,v_q,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
plot(s_q,estimateStates(1,:),"LineWidth",2,"Color","red","LineStyle","--");
legend('Model','Pozorovatel','FontSize',font_size);
hold off;

figure("Name","Výška");
plot(s_q,z_q,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('výška [m]','FontSize',font_size);
plot(s_q,estimateStates(2,:),"LineWidth",2,"Color","red","LineStyle","--");
legend('Model','Pozorovatel','FontSize',font_size);
hold off;

rmse(estimateStates(3,:),alpha_q)

%% State model of tram without process noise
function stateNext = stateModel(state,ds,F_m_q,m,K,c_r)
    g = 9.81;
    % nadmorska vyska = 273m, T = 10C 
    dvds = F_m_q/(m*state(1))-((1/(2*m))*(K*state(1)))-(g/state(1))*(c_r+sin(state(3)));
    v_k = state(1) + ds*dvds;
    z_k = state(2) + ds*sin(state(3)) ;
    a_k = state(3);
    stateNext = [v_k;z_k;a_k];
end

%% output of model 
function z = measureModel(state)
    H = [1 0 0; 
         0 1 0];    
    z = H*state;
end

% State transition Jacobian:
function jacobian = jacobState(state,ds,F_m_q,m,K,c_r)
    % Validate that state is 4-elements long and that there are two inputs
    g = 9.81;
    jacobian = [1+ds*(-F_m_q/(m*state(1)^2)+((c_r+sin(state(3))*g)/state(1)^2)-((1/2*K)/m)) 0 -(ds*g*cos(state(3)))/state(1); 
           0 1 ds*cos(state(3)); 
           0 0 1];
end

