import pandas as pd
import folium

df = pd.read_csv('weird_places.csv', usecols=['Latitude', 'Longitude'])

map_center = [50.0755, 14.4378]  # Například pro Prahu.


m = folium.Map(location=map_center, zoom_start=13)

for index, row in df.iterrows():
    folium.Marker(
        location=[row['Latitude'], row['Longitude']]
    ).add_to(m)

m.save('map.html')  # Uložení mapy do HTML souboru
m
