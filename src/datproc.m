% data loading
dat = readtable("emu16_08_12_52.csv", "ExpectedNumVariables", 6);
accelZ = table2array(dat(:, 6));
accelY = table2array(dat(:, 5));
d2 = table2array(dat(:, 2));

% Fourier transform to see frequency spectrum
d6fft = fft(d6, 2000);
figure(1)
plot(accelZ)

% Gyroscope output integration, gives us measured angle profile
d2int = array_int(d2);

figure(2)
% Constant length average filtering (window filtering)
d6win = myWindow(accelZ, 1000);
plot(d6win)
d2win = myWindow(d2, 1000);
% + integral
d2winint = array_int(d2win);


figure(3)
% Butterworth low-pass filtering
d6but = myLowpass(accelZ);
d6butfft = fft(d6but, 2000);
plot(d6but)

figure(4)
% Butterworth filtering on window-filtered signal
d6winbut = myLowpass(d6win);
d6winbutfft = fft(d6winbut, 2000);
plot(d6winbut)

% External filter (see below)
denoise();

figure(6)
% Median filter
d6med = medfilt1(accelZ, 1000);
plot(d6med)

figure(7)
% Window filter used on median-filtered signal
d6medwin = myWindow(d6med, 2000);
plot(d6medwin)

% filtered and integrated gyroscope data conv. to degs (as final output)
d6fin = deg2rad(d2winint);
figure(8);
plot(d6fin)

figure(9)
% Gyro and accelerometric data fusion
datafuse = fuse(deg2rad(d2win), atan2(accelY, accelZ));
plot(datafuse)

figure(10)
% Angle profile obtained by gyroscope integral
gyroZ = table2array(dat(:, 2));
gyroZfil = array_int(myWindow(gyroZ, 2000));

% Butterworth low-pass filter
function y = myLowpass(x)
[b,a] = butter(5,0.001);
y = filter(b, a, x);
end

% Constant-length-average filter (window filter)
function y = myWindow(x, windowSize)
b = (1/windowSize)*ones(1,windowSize);
a = 1;
y = filter(b, a, x);
end

% Gyroscope and accelerometer fusing using complementary function
function y = fuse(gyrData, accData)
y(1:length(gyrData)) = 0;
y(1) = 0;
Ts = 1/2000;
for i = 2:length(gyrData)
    y(i) = 0.98*(y(i-1) + gyrData(i)*Ts) + 0.02*accData(i);
end
end

% Numerical integration
function y = array_int(x)
u(1:length(x)) = 0;
u(1) = x(1);
for i = 2:length(x)
    u(i) = u(i-1) + x(i);
end
y = u;
end

%%% Filter using approach from site
% https://dsp.stackexchange.com/questions/3026/picking-the-correct-filter-for-accelerometer-data
function denoise()
dat = readtable("emu16_08_12_52.csv", "ExpectedNumVariables", 6);
f = table2array(dat(:, 6));
g = f;
fc = denoisetv(g,.1);
figure(5);
plot(fc,'g');
title('De-noised');
axis([1 length(f) -4 4]);
end

function f = denoisetv(g,mu)
I = length(g);
u = zeros(I,1);
y = zeros(I,1);
rho = 30;

eigD = abs(fftn([-1;1],[I 1])).^2;
for k=1:100
    f = real(ifft(fft(mu*g+rho*Dt(u)-Dt(y))./(mu+rho*eigD)));
    v = D(f)+(1/rho)*y;
    u = max(abs(v)-1/rho,0).*sign(v);
    y = y - rho*(u-D(f));
end
end

function y = D(x)
y = [diff(x);x(1)-x(end)];
end

function y = Dt(x)
y = [x(end)-x(1);-diff(x)];
end
%%%
