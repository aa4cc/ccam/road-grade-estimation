%{
plotting all together for conclusion
loads all the necessary data and compares them
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clc,clear;

load("profil_a\EKF_data_profil_a.mat");
etalon_EKF_a = rad2deg(alpha_q);
alpha_EKF_a = rad2deg(estimateStates(3,:));
s_EKF_a = s_q;

load("profil_a\luenberg_data_profil_a.mat");
alpha_l_a = alpha_2;
s_l_a = x_s;

load("profil_h\EKF_data_profil_h.mat");
etalon_EKF_h = rad2deg(alpha_q);
alpha_EKF_h = rad2deg(estimateStates(3,:));
s_EKF_h = s_q;

load("profil_h\luenberg_data_profil_h.mat");
alpha_l_h = alpha_2;
s_l_h = x_s;

load('profil_h\EKF_average_profil_h.mat')
alpha_average_h = rad2deg(optimalState); 
s_a_h = s_q;

load('profil_a\average_data_profil_a.mat')
alpha_average_a = rad2deg(optimalState); 
s_a_a = s_q;

font_size = 14;

figure("Name","Alfa h");
plot(s_l_h(1:5265),alpha_l_h(1:5265),"LineWidth",0.8,"Color","red");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
plot(s_EKF_h(1:1361),alpha_EKF_h(1:1361),"LineWidth",0.8,"Color","yellow");
plot(s_a_h,alpha_average_h,"LineWidth",0.8,"Color","blue");
plot(s_EKF_h(1:1361),etalon_EKF_h(1:1361),"LineWidth",0.8,"Color","green");
legend('Luenberg','EKF','EKF průměrování','Etalon','FontSize',font_size);
hold off;

figure("Name","Alfa a");
plot(s_l_a(1:28450),alpha_l_a(1:28450),"LineWidth",0.8,"Color","red");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
plot(s_EKF_a(1:8001),alpha_EKF_a(1:8001),"LineWidth",0.8,"Color","yellow");
plot(s_a_a,alpha_average_a,"LineWidth",0.8,"Color","blue");
plot(s_EKF_a(1:8001),etalon_EKF_a(1:8001),"LineWidth",0.8,"Color","green");
legend('Luenberg','EKF','EKF průměrování','Etalon','FontSize',font_size);
hold off;
