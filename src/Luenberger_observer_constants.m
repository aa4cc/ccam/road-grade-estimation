%{
constants for Luenberger-observer simulink
height
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clear;
konstanty_model;

% 0.06666
x_s = out.x_s.signals.values;
alpha_1 = out.alpha_hat.signals(1).values;
alpha_2 = out.alpha_hat.signals(2).values;

v_1 = out.v_hat.signals(1).values;
v_2 = out.v_hat.signals(2).values;

v_ref = out.v_ref.signals.values;

font_size = 14;
figure("Name","Alfa");
plot(x_s,v_ref,"LineWidth",0.5,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
lgd = legend('Referenční rychlost');
fontsize(lgd,font_size,'points');
hold off;

figure("Name","Alfa");
plot(x_s,alpha_1,"LineWidth",2,"Color","blue");
hold on;
grid on;
plot(x_s,alpha_2,"LineWidth",2,"LineStyle","--","Color","red");
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
lgd = legend('Model','Pozorovatel');
fontsize(lgd,font_size,'points');
hold off;

figure("Name","Rychlost");
plot(x_s,v_1,"LineWidth",2,"Color","blue");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('rychlost [m/s]','FontSize',font_size);
lgd = legend('Model');
fontsize(lgd,font_size,'points');
hold off;

rmse(alpha_1,alpha_2)