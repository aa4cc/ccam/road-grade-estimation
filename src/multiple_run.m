%{
implementation of multiple runs over same road algorithm for avereging
Codes used for bachelor thesis road grade estimation by onboard multisensor fusion 
contains:

@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2023
%}

clc,clear;


vlozeni_profilu;
v_r = 0;
t_r = 0;
konstanty_model;
EKF_no_plot;


% r = a + (b-a).*rand(N,1)."
n_run = 300;
a_v = -5;
b_v = 5;
v_ref = a_v + (b_v-a_v) .* rand(1,n_run);

a_t = -9.9;
b_t = 10;
t_ref = a_t + (b_t-a_t) .* rand(1,n_run);

database = NaN(n_run+1,length(estimateStates(3,:)));
database(1,:) = estimateStates(3,:);
figure("Name","Alfa");
plot(s_q,rad2deg(alpha_q));
hold on;
clearvars -except v_ref t_ref database simIn n_run;

for i = 1:n_run
    simIn = setVariable(simIn,'v_r',v_ref(i));
    simIn = setVariable(simIn,'t_r',t_ref(i));
    konstanty_model;
    vlozeni_profilu;
    out = sim(simIn);
    idx = i+1;
    fprintf("V iteraci: %d je hodnota v_r je %f a t_r je %f\n",idx,v_ref(i),t_ref(i));
    EKF_no_plot;
    database(idx,:) = estimateStates(3,:);
    plot(s_q,rad2deg(estimateStates(3,:)));
    clearvars -except v_ref t_ref database simIn n_run;
end


legend_2 = "zprůměrování " + n_run + " iterací";

optimalState = sum(database)/(n_run+1);

konstanty_model;
vlozeni_profilu;
EKF_no_plot;
fprintf("Hodnota rmse pro jedno spuštění = %f\n",rmse(estimateStates(3,:),alpha_q));
fprintf("Hodnota rmse pro %d spuštění = %f\n",n_run+1,rmse(optimalState,alpha_q));

font_size = 12;

figure("Name","Vyhlazení pomocí průměrování");
plot(s_q,rad2deg(estimateStates(3,:)),"LineWidth",0.5,"Color","red","LineStyle","--");
hold on;
grid on;
xlabel('s [m]','FontSize',font_size);
ylabel('sklon [°]','FontSize',font_size);
plot(s_q,rad2deg(optimalState),"LineWidth",2,"Color","blue");
plot(s_q,rad2deg(alpha_q),"LineWidth",2,"Color","green");
legend('1.iterace',legend_2,'Pravý profil','FontSize',11);
hold off;
