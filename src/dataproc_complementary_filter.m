clc;clear;

dat = readtable("emu16_08_13_30.csv", "ExpectedNumVariables", 6);

x_gyro = deg2rad(table2array(dat(:,1)));
y_gyro = deg2rad(table2array(dat(:,2)));
z_gyro = deg2rad(table2array(dat(:,3)));
x_accel = table2array(dat(:,4));
y_accel = table2array(dat(:,5));
z_accel = table2array(dat(:,6));

gyro = [x_gyro y_gyro z_gyro];
accel = [x_accel y_accel z_accel];
Fs = 2000;
fuse = complementaryFilter('SampleRate', Fs,'HasMagnetometer',false,'AccelerometerGain',0.05);
q = fuse(accel, gyro);
figure(2)
data = eulerd(q, 'ZYX', 'frame').*(-1);
plot(data(:,2),'LineWidth',2);
grid on;
title('Orientation Estimate');
legend('Z-rotation', 'Y-rotation', 'X-rotation');
ylabel('Degrees');