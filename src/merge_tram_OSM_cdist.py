"""
Název: merge_tram_OSM_cdist
Popis:
Autor: Radek Chládek
Datum: 21.09.2023
Verze: 1.0
Copyright (c) 2023 Radek Chládek

Tento program je svobodný software: můžete ho šířit a upravovat podle
Podmínek Svobodné licence GNU, publikované nadací Svobodný software,
verze 3 nebo (dle vašeho uvážení) jakékoliv novější verze.

Tento program je distribuován v naději, že bude užitečný,
ale BEZ JAKÉKOLIV ZÁRUKY; i bez záruky OBCHODOVATELNOSTI nebo VHODNOSTI
PRO KONKRÉTNÍ ÚČELY. Více detailů naleznete v
Svobodné licenci GNU.

Tuto licenci jste měli obdržet spolu s tímto programem.
Pokud ne, podívejte se na http://www.gnu.org/licenses/.
"""

import pandas as pd
from scipy.spatial.distance import cdist
import numpy as np
import time

start_time = time.time()  # Začátek měření času

# read altitude with gps from OSM and GEOPORTAL
df1 = pd.read_csv('altitude.csv')

# read road_grade_dataset from tram
df2 = pd.read_csv('road_grade_dataset.csv')

# Vytvořte pole s GPS koordinacemi pro oba soubory
coords1 = df1[['Latitude', 'Longitude']].values
coords2 = df2[['Latitude', 'Longitude']].values

# Vypočítejte vzdálenosti mezi body v obou souborech
distances = cdist(coords1, coords2, metric='euclidean')  # Můžete použít jinou metriku podle potřeby


# Najděte indexy nejbližších bodů v df2 pro každý bod v df1
closest_indices = np.argmin(distances, axis=1)

# Přidejte zbylé sloupce z df2 k df1 pro nejbližší body
nejblizi_data = df2.iloc[closest_indices]

# Resetujte indexy nejbližších bodů, abyste mohli provést sloučení
nejblizi_data.reset_index(drop=True, inplace=True)

df1 = pd.concat([df1, nejblizi_data], axis=1)

# Uložte výsledek do nového CSV souboru
df1.to_csv('merged_cdist.csv', index=False)

end_time = time.time()    # Konec měření času
elapsed_time = end_time - start_time
print(f"Čas vykonání kódu: {elapsed_time} sekund")
