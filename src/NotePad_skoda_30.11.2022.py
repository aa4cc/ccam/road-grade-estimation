'''
notepad for quick entries during our Skoda experiment 
@author: Radek Chládek
@contact: chladrad@fel.cvut.cz
@copyright: (c) 2022
'''

import datetime

# Append-adds at last
# append mode
file = open("experiment_30_11_2022.txt", "a")

running = True

while(running):
    # q - manipulace se senzory [1]
    # s - zastaveni [2]
    # sk - konec stani
    # o - otres [3]
    # ok - konec otres
    # r - otevreny prostranstvi [4]
    # z - zatacky [5]
    # zk - konec zatacky
    # p - zacatek nejakeho experimentu [6]
    # m - end of program [7]
    # kn - kopec nahoru
    # kd - kopec dolu
    # kk - konec kopec
    # ap - acceleration plus
    # am - acceleration minus
    # ak - end of acceleration
    #  f - zacatek rovinky
    # fk - konec rovinky

    type_of_msg = input("Type of message: ")
    time_stemp = datetime.datetime.now()
    intro_msg = "Something went wrong"
    if type_of_msg == "q":
        intro_msg = "[1]"
    elif type_of_msg == "s":
        intro_msg = "[2]"
    elif type_of_msg == "sk":
        intro_msg = "[3]"
    elif type_of_msg == "o":
        intro_msg = "[4]"
    elif type_of_msg == "ok":
        intro_msg = "[5]"
    elif type_of_msg == "r":
        intro_msg = "[6]"
    elif type_of_msg == "z":
        intro_msg = "[7]"
    elif type_of_msg == "zk":
        intro_msg = "[8]"
    elif type_of_msg == "p":
        intro_msg = "[9]"
    elif type_of_msg == "kn":
        intro_msg = "[10]"
    elif type_of_msg == "kd":
        intro_msg = "[11]"
    elif type_of_msg == "kk":
        intro_msg = "[12]"
    elif type_of_msg == "ap":
        intro_msg = "[13]"
    elif type_of_msg == "am":
        intro_msg = "[14]"
    elif type_of_msg == "ak":
        intro_msg = "[15]"
    elif type_of_msg == "f":
        intro_msg = "[16]"
    elif type_of_msg == "fk":
        intro_msg = "[17]"
    elif type_of_msg == "m":
        intro_msg = "[18]"
        running = False
    else:
        intro_msg = "Spatne zadany typ zpravy: "
    new_line = input("Write: ")
    file.write(str(time_stemp) + ", " + intro_msg + ", " + new_line +"\n")

file.close()
